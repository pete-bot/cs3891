/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Sample/test code for running a user program.  You can use this for
 * reference when implementing the execv() system call. Remember though
 * that execv() needs to do more than runprogram() does.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <lib.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vm.h>
#include <vfs.h>
#include <syscall.h>
#include <test.h>
#include <kern/fcntl.h>
#include <copyinout.h>
#include <synch.h>
#include <thread.h>

/*
 * Load program "progname" and start running it in usermode.
 * Does not return except on error.
 *
 * Calls vfs_open on prognjame and thus may destroy it.
 */



int
runprogram(char *progname)
{
	struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;

	/* Open the file. */
	result = vfs_open(progname, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* We should be a new process. */
	KASSERT(proc_getas() == NULL);

	/* Create a new address space. */
	as = as_create();
	if (as == NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	proc_setas(as);
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}



	//#############################################
	// 				OUR STUFF
	//#############################################

	struct proc *proc = curproc;


	// create file descriptor for stdinput
	char filename[] = "con:";
	struct vnode *v1;
	result = vfs_open(filename, O_RDONLY, 0664, &v1);
	
	if (result) {
		return result;
	}

	struct file_desc_t *file1 = kmalloc(sizeof(struct file_desc_t));
	file1->name = kstrdup(filename);
	file1->ref_count = 1;
	file1->flags = O_RDONLY;
	file1->v_node_ptr = v1;
	file1->offset_pos = 0;
	file1->fd_lock = lock_create("STDIN_lock");
	proc->fd_table[0] = file1;
	




	// create file ddescriptor for stdout
	struct vnode *v2;
	char filename2[] = "con:";
	result = vfs_open(filename2, O_WRONLY, 0664, &v2);

	if (result) {
		return result;
	}

	struct file_desc_t *file2 = kmalloc(sizeof(struct file_desc_t));
	file2->name = kstrdup(filename2);
	file2->ref_count = 1;
	file2->flags = O_WRONLY;
	file2->v_node_ptr = v2;
	file2->offset_pos = 0;
	file2->fd_lock = lock_create("STDOUT_lock");
	proc->fd_table[1] = file2;
	

	// create file descriptor for stderr
	struct vnode *v3;
	char filename3[] = "con:";
	result = vfs_open(filename3, O_WRONLY, 0664, &v3);
	if (result) {
		return result;
	}
	struct file_desc_t *file3 = kmalloc(sizeof(struct file_desc_t));
	file3->name = kstrdup(filename3);
	file3->ref_count = 1;
	file3->flags = O_WRONLY;
	file3->v_node_ptr = v3;
	file3->offset_pos = 0;
	file3->fd_lock = lock_create("STDERR_lock");
	proc->fd_table[2] = file3;


	//#############################################
	// 				END OUR STUFF
	//#############################################



	/* Warp to user mode. */
	enter_new_process(0 /*argc*/, NULL /*userspace addr of argv*/,
			  NULL /*userspace addr of environment*/,
			  stackptr, entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
}

