
#include <thread.h>
#include <synch.h>
#include <kern/limits.h>

#ifndef _PROCESS_CALLS_H_
#define _PROCESS_CALLS_H_

struct process_status {
	pid_t p_pid; //Peters pid
	pid_t parent_pid;
	struct semaphore *wait_sem;
	bool exited;
	int exit_code; 
	struct proc *p_self;
};

struct process_status* assign_pid(void);
void release_pid(pid_t pid);
int get_processes_count(void);



extern struct process_status* processes[PROC_MAX];
extern int processes_count;
extern struct lock *process_lock;

#endif // _PROCESS_CALLS_H_