#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <addrspace.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <proc.h>

#include <process_calls.h>
#include <kern/wait.h>

#include <../arch/mips/include/trapframe.h>
static int proc_sys_close(int fd, int32_t *retval,struct proc *proc);
static void proc_proc_destroy(struct proc *proc);


struct process_status* processes[PROC_MAX];
int processes_count = 0;
struct lock *process_lock;

//#########################################################################
//						      OUR PROCESS CALLS
//#########################################################################


int sys_fork(struct trapframe* parent_tf, void (*entrypoint)(void *data1, unsigned long data2), int32_t *retval)
{
	struct proc *proc = curproc;
	
	struct trapframe* child_tf = kmalloc(sizeof(struct trapframe));
	if(child_tf == NULL){
		return ENOMEM;
	}
	*child_tf = *parent_tf;

	struct addrspace* new_as;
	int err = as_copy(proc->p_addrspace, &new_as );
	if(err){
		kfree(child_tf);
		return err;
	}

	// create a new process object
	struct proc *new_proc = proc_create_runprogram(proc->p_name);
	if(new_proc == NULL){
		kfree(child_tf);
		return ENOMEM;
	}

	// fd copy
	int *err_ptr = memcpy( new_proc->fd_table, curproc->fd_table, 
		sizeof(struct file_desc_t*)*OPEN_MAX );
	if(err_ptr == NULL){
		proc_proc_destroy(new_proc);
		return ENOMEM;
	}
	// set fd count correctly
	new_proc ->fd_table_count = curproc->fd_table_count;
	new_proc ->p_status -> parent_pid = proc->p_status->p_pid;

	for(int i = 0; i < OPEN_MAX; ++i){
		if(new_proc->fd_table[i] != NULL) {
			lock_acquire(new_proc->fd_table[i]->fd_lock);
			new_proc->fd_table[i]->ref_count++;
			lock_release(new_proc->fd_table[i]->fd_lock);
	
		}
	}

	// call thread fork
	err = thread_fork(proc->p_name, new_proc , 
		entrypoint, (void*)child_tf, (unsigned long) new_as );
	if(err){
		proc_proc_destroy(new_proc);
		return err;
	}

	*retval = new_proc->p_status->p_pid;


	return 0;
}

int sys_getpid(int * retval)
{
	
	struct proc *proc = curproc;
	*retval = proc->p_status->p_pid;
	return 0;
}

struct process_status* assign_pid(void) {


	if(processes_count == PROC_MAX) {
		return NULL;
	}

	if(process_lock != NULL) {
		lock_acquire(process_lock);
	}
	
	int pid_i = 1;
	while(processes[pid_i] != NULL) {
		
		if(pid_i == PROC_MAX) {
			pid_i = 1;
		} else {
			pid_i++;
		}
	}

	processes[pid_i] = kmalloc(sizeof(struct process_status)); 
	if(processes[pid_i] == NULL) {
		kprintf("ERROR: pid assignment failed\n");
		if(process_lock != NULL) {
			lock_release(process_lock);
		}
		return NULL;
	}
	processes[pid_i]->p_pid = pid_i;
	processes_count++;
	processes[pid_i]->parent_pid = -1;
	processes[pid_i]->wait_sem = sem_create("wait_sem",0);
	processes[pid_i]->exited = 0;
	processes[pid_i]->exit_code = -1;

	if(process_lock != NULL) {
		lock_release(process_lock);	
	}


	return processes[pid_i];

}


int sys_waitpid(pid_t pid, userptr_t status, int options, int32_t *retval)
{
	
	
	struct proc* proc = curproc;
	int null_flag = 0;
	if(status == NULL){
		null_flag = 1;
	}

	if( (options != WNOHANG) && (options != WUNTRACED) && (options != 0) ){
		return EINVAL;
	}

	if( (pid < 0 || pid > PROC_MAX) || (processes[pid] == NULL) ){
		return ESRCH;
	}

	lock_acquire(process_lock);
	//  	parent of pid  		is not    current pid 
	if( processes[pid]->parent_pid != proc->p_status->p_pid ){
		lock_release(process_lock);
		return ECHILD;
	}
	
	if(processes[pid]->exited){

		if(!null_flag) {
			void* exit_code = &(processes[pid]->exit_code);
			int err = copyout(exit_code, status, sizeof(int));
			if(err) {
				lock_release(process_lock);
				return err;		
			}
		}
		
		*retval = (int32_t)pid;
		
		lock_release(process_lock);
		proc_proc_destroy(processes[pid]->p_self);
	
		return 0;
	}	

	lock_release(process_lock);
	
	while(1) {
		
		if(processes[pid]->exited){
			
			if(!null_flag) {
				void* exit_code = &(processes[pid]->exit_code);
				int err = copyout(exit_code, status, sizeof(int));
				if(err) {
					return err;		
				}
			}
			*retval = (int32_t)pid;
			proc_proc_destroy(processes[pid]->p_self);
			
			return 0;
		}
		
		P(processes[pid]->wait_sem);	
	}

	return 0;
}


void release_pid(pid_t pid) 
{

	
	if(process_lock != NULL) {
		lock_acquire(process_lock);
	}

	if(pid < 0 || pid >= PROC_MAX || processes[pid] == NULL) {
		
		if(process_lock != NULL) {
			lock_release(process_lock);
		}
		return; 
	}
	sem_destroy(processes[pid]->wait_sem);
	kfree(processes[pid]);

	processes[pid] = NULL;

	processes_count--;

	if(process_lock != NULL) {
		lock_release(process_lock);
	}

}

int get_processes_count(void){
	return processes_count;
}

void sys_exit(int exitcode)
{
	struct proc *proc = curproc;
	lock_acquire(process_lock);
	proc->p_status->exit_code = _MKWAIT_EXIT(exitcode);
	proc->p_status->exited = 1;
	lock_release(process_lock);
	V(proc->p_status->wait_sem);

	thread_exit();	


}

static int proc_sys_close(int fd, int32_t *retval,struct proc *proc)
{

	int release_flag = 1;

	if( fd < 0 || fd > 31 || proc->fd_table[fd] == NULL ){ 
		return EBADF;
	}

	lock_acquire(proc->fd_table[fd]->fd_lock);

	struct file_desc_t *file = proc->fd_table[fd];

	file->ref_count--;
	if(file->ref_count == 0) {

		vfs_close(file->v_node_ptr);

		kfree(file->name);
		lock_release(proc->fd_table[fd]->fd_lock);
		lock_destroy(file->fd_lock);
		kfree(file);

		release_flag = 0;

	}

	proc->fd_table_count--;
	*retval = 0;
	
	
	if(release_flag){
		lock_release(proc->fd_table[fd]->fd_lock);
	}

	proc->fd_table[fd] = NULL;

	return 0;
}

static void
proc_proc_destroy(struct proc *proc)
{


	KASSERT(proc != NULL);
	KASSERT(proc != kproc);
	
	/* VFS fields */
	if (proc->p_cwd) {
		VOP_DECREF(proc->p_cwd);
		proc->p_cwd = NULL;
	}

	/* VM fields */
	if (proc->p_addrspace) {
		
		struct addrspace *as;

		if (proc == curproc) {
			as = proc_setas(NULL);
			as_deactivate();
		}
		else {
			as = proc->p_addrspace;
			proc->p_addrspace = NULL;
		}
		as_destroy(as);
	}
	


	// need to close all open files
	// deallocate file desriptor 
	int32_t retval = 0;
	for(int i = 0; i < OPEN_MAX; ++i){
		if(proc->fd_table[i] != NULL) {
			int result = proc_sys_close(i, &retval,proc);
			if(result) {
				kprintf("I Thought we were friends!");
			}
			
		}
	}

	KASSERT(proc->p_numthreads == 0);
	spinlock_cleanup(&proc->p_lock);

	release_pid(proc->p_status->p_pid);
	kfree(proc->p_name);
	kfree(proc);
}

