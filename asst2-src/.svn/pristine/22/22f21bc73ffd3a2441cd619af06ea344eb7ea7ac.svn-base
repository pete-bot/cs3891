#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <addrspace.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <syscall.h>
#include <copyinout.h>
#include <proc.h>


/*
 * Add your file-related functions here ...
 */
// open, read, write, lseek, close, dup2.


int sys_open(userptr_t filename, int flags, int *retval)
{


	struct proc *proc = curproc;

	// check for issues with open syscall
	if(filename == NULL){
		return EFAULT;
	}

	if(proc->fd_table_count == OPEN_MAX){
		return EMFILE;
	}

	int flag_check  = flags & (O_RDONLY + O_WRONLY + O_RDWR);
	if(flag_check < 0 || flag_check >2){
		return EINVAL;

	}
	
	int typecheck = flags & (O_CREAT + O_EXCL + O_TRUNC + O_APPEND);

	int fd_pos;
	for(fd_pos = 0; fd_pos < OPEN_MAX; ++fd_pos){
		if(proc->fd_table[fd_pos] == NULL) {break;}

	}

	char name_buf[NAME_MAX];
	int error;
	size_t return_size;
	error = copyinstr(filename,name_buf,NAME_MAX,&return_size);
	if(error != 0) {
		return error;
	}

	struct vnode *ret;
	int open_error = vfs_open(name_buf,flags,0,&ret);
	if(open_error != 0) {
		return open_error;
	}

	struct file_desc_t *file = kmalloc(sizeof(struct file_desc_t));

	file->name = kstrdup(name_buf);
	file->ref_count = 1;
	file->flags = flags;
	file->fd_lock = lock_create("fd_lock");

	file->v_node_ptr = ret;
	if(typecheck == O_APPEND) {
		struct stat st;
		int vop_error = VOP_STAT(ret,&st);
		if(vop_error != 0) {
			return vop_error;
		}

		file->offset_pos = st.st_size;
	} else {
		file->offset_pos = 0;
	}

	proc->fd_table[fd_pos] = file;
	proc->fd_table_count++;
	*retval = fd_pos;


	if(proc->fd_table[fd_pos]->fd_lock == NULL) {
		kprintf(">>> error: fd_lock was NULL.\n");
	}

	return 0;

}


int sys_write(int fd, const void *buf, size_t nbytes, int32_t  *retval)
{

	struct proc *proc = curproc;

	if( fd < 0 || fd > 31 || proc->fd_table[fd] == NULL ){ 
		return EBADF;
	}

	if( !( (proc->fd_table[fd]->flags & O_WRONLY) == O_WRONLY 
		|| ( (proc->fd_table[fd]->flags & O_RDWR) == O_RDWR)) ){
		return EBADF;
	}  


	lock_acquire(proc->fd_table[fd]->fd_lock);

	struct iovec iov;
	struct uio u;

	iov.iov_ubase = (userptr_t)buf;
	iov.iov_len = nbytes;			 
	u.uio_iov = &iov;
	u.uio_iovcnt = 1;
	u.uio_resid = nbytes;          
	u.uio_offset = proc->fd_table[fd]->offset_pos;
	u.uio_segflg = UIO_USERSPACE;
	u.uio_rw = UIO_WRITE;
	u.uio_space = proc->p_addrspace;

	//kprintf("Process printing: %s \n", proc->p_name);

	int result = VOP_WRITE(proc->fd_table[fd]->v_node_ptr, &u);
	if (result) {
		lock_release(proc->fd_table[fd]->fd_lock);
		return result;
	}

	*retval = nbytes - u.uio_resid;
	proc->fd_table[fd]->offset_pos += *retval;

	lock_release(proc->fd_table[fd]->fd_lock);
	return 0;

}


int sys_read(int fd, const void *buf, size_t nbytes, int32_t  *retval)
{

	struct proc *proc = curproc;

	if( fd < 0 || fd > 31 || proc->fd_table[fd] == NULL ){ 
		return EBADF;
	}

	if( (proc->fd_table[fd]->flags & O_WRONLY) == O_WRONLY){
		return EBADF;
	}  

	lock_acquire(proc->fd_table[fd]->fd_lock);

	struct iovec iov;
	struct uio u;


	iov.iov_ubase = (userptr_t)buf;
	iov.iov_len = nbytes;			 // length of the memory space
	u.uio_iov = &iov;
	u.uio_iovcnt = 1;
	u.uio_resid = nbytes;          // amount to read from the file
	u.uio_offset = proc->fd_table[fd]->offset_pos;
	u.uio_segflg = UIO_USERSPACE;
	u.uio_rw = UIO_READ;
	u.uio_space = proc->p_addrspace;

	int result = VOP_READ(proc->fd_table[fd]->v_node_ptr, &u);
	if (result) {
		lock_release(proc->fd_table[fd]->fd_lock);
		return result;
	}

	*retval = nbytes - u.uio_resid;
	proc->fd_table[fd]->offset_pos += *retval;

	lock_release(proc->fd_table[fd]->fd_lock);

	return 0;

}

int sys_close(int fd, int32_t *retval)
{
	struct proc *proc = curproc;

	int release_flag = 1;

	if( fd < 0 || fd > 31 || proc->fd_table[fd] == NULL ){ 
		return EBADF;
	}

	lock_acquire(proc->fd_table[fd]->fd_lock);

	struct file_desc_t *file = proc->fd_table[fd];

	file->ref_count--;
	if(file->ref_count == 0) {

		vfs_close(file->v_node_ptr);

		kfree(file->name);
		lock_release(proc->fd_table[fd]->fd_lock);
		lock_destroy(file->fd_lock);
		kfree(file);

		release_flag = 0;

	}

	proc->fd_table_count--;
	*retval = 0;
	
	
	if(release_flag){
		lock_release(proc->fd_table[fd]->fd_lock);
	}

	proc->fd_table[fd] = NULL;

	return 0;
}

int sys_lseek(int fd, off_t pos, int whence, off_t *retval) {

	struct proc *proc = curproc;

	if( fd < 0 || fd > 31 || proc->fd_table[fd] == NULL ){ 
		return EBADF;
	}

	if(whence != SEEK_SET && whence != SEEK_CUR && whence != SEEK_END) {
		return EINVAL;
	}

	if(pos + proc->fd_table[fd]->offset_pos < 0) {
		return EINVAL;
	}

	if(!VOP_ISSEEKABLE(proc->fd_table[fd]->v_node_ptr)) {
		return ESPIPE;
	}

	lock_acquire(proc->fd_table[fd]->fd_lock);

	if(whence == SEEK_SET) {
		proc->fd_table[fd]->offset_pos = pos;
	} else if (whence == SEEK_CUR) {
		proc->fd_table[fd]->offset_pos+= pos;
	} else {
		struct stat st;
		int vop_error = VOP_STAT(proc->fd_table[fd]->v_node_ptr,&st);
		if(vop_error != 0) {
			lock_release(proc->fd_table[fd]->fd_lock);
			return vop_error;
		}
		proc->fd_table[fd]->offset_pos = st.st_size + pos;
	}

	*retval = proc->fd_table[fd]->offset_pos;
	lock_release(proc->fd_table[fd]->fd_lock);
	return 0;
}

int sys_dup2(int oldfd, int newfd, int *retval) {

	struct proc *proc = curproc;

	if( oldfd < 0 || oldfd > 31 || proc->fd_table[oldfd] == NULL ){ 
		return EBADF;
	}

	if( newfd < 0 || newfd > 31){ 
		return EBADF;
	}

	if(proc->fd_table_count == OPEN_MAX){
		return EMFILE;
	}

	if(proc->fd_table[newfd] == NULL){
		proc->fd_table_count++;
	}

	proc->fd_table[newfd] = proc->fd_table[oldfd];
	lock_acquire(proc->fd_table[oldfd]->fd_lock);
	proc->fd_table[newfd]->ref_count++;
	*retval = newfd; 

	lock_release(proc->fd_table[oldfd]->fd_lock);

	return 0;

}

