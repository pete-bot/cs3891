/*
 * Declarations for file handle and file table management.
 */

#ifndef _FILE_H_
#define _FILE_H_

/*
 * Contains some file-related maximum length constants
 */
#include <limits.h>
#include <thread.h>
#include <synch.h> 

struct file_desc_t{
	char *name;
	int ref_count;
	int flags;
	off_t offset_pos;
	struct vnode* v_node_ptr;
	struct lock *fd_lock;
};

#endif /* _FILE_H_ */
