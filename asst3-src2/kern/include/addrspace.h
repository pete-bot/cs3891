/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _ADDRSPACE_H_
#define _ADDRSPACE_H_

/*
 * Address space structure and operations.
 */


#include <vm.h>
#include "opt-dumbvm.h"

struct vnode;

/*
 * Address space - data structure associated with the virtual memory
 * space of a process.
 *
 * You write this.
 */

#define NUM_PAGES 1024
#define MAX_ADDRESSABLE 1024

#define REGION_WRITEABLE 1
#define REGION_READABLE 2
#define REGION_EXECUTABLE 4

#define NOT_IGNORE_REF true
#define IGNORE_REF false

// macros for accessing permission bits
#define GET_WRITEABLE(x) ((x & REGION_WRITEABLE) == REGION_WRITEABLE) 
#define SET_WRITEABLE(x) (x |= REGION_WRITEABLE)

#define GET_READABLE(x) ((x & REGION_READABLE) == REGION_READABLE) 
#define SET_READABLE(x) ( x |= REGION_READABLE)

#define GET_EXECUTABLE(x) ((x & REGION_EXECUTABLE) == REGION_EXECUTABLE) 
#define SET_EXECUTABLE(x) (x |= REGION_EXECUTABLE)


// macros for getting offsets/addresses from vaddr

#define GET_L1_ADDR(x) (x>>22)
#define GET_L2_ADDR(x) ((x<<10)>>22)
#define GET_OFFSET_ADDR(x) ((x<<20)>>20)

#define HEAP_OFFSET 1
#define DEFAULT_STACK 16

 
/* page struct */
struct logical_page_t{
    paddr_t physical_address;
    struct spinlock page_lock;
};

/* Page table entry level 1 */
struct pt_entry{
    struct logical_page_t **page_L2;  
};


//################################################################
//              REGION DATA/FN PROTOTYPES
//################################################################


typedef struct region_t* region_ptr;
typedef struct region_list_t* region_list_ptr;

struct region_list_t{
    region_ptr first;
    region_ptr last;
    region_ptr stack;
    region_ptr heap;
    vaddr_t heap_start;
    vaddr_t heap_end;
    int num_regions;
};


struct region_t{
    vaddr_t base;
    size_t size;
    region_ptr next;
    uint8_t permissions;
    uint8_t prev_permissions;
    size_t num_pages;

};

// place a new region node at the end of the region_list
// if region_list is empty (new region_list) this will create a new first region
// returns the new region ptr created. 
region_ptr insert_region(region_list_ptr region_list, vaddr_t base_in, size_t size, int permissions_in, size_t num_pages);
bool base_region_exists(vaddr_t base,region_list_ptr region_list);
uint8_t get_region_permission(vaddr_t address, region_list_ptr region_list); 

// region functions
region_list_ptr create_region_list(void);
region_ptr create_node(vaddr_t base_in, size_t size_in, int permissions_in, size_t n_pages);
void destroy_region_list(region_list_ptr);
bool page_walk(int *l1, int *l2,int *offset, struct pt_entry **l1_entry,struct logical_page_t **logical_page, vaddr_t vaddr);
bool is_in_region(vaddr_t address, region_list_ptr region_list);
int copy_region_list(region_list_ptr old, region_list_ptr new_list );
void init_page_L2(struct logical_page_t **page_L2);
void init_page_L1(struct pt_entry **page_L1);

void clean_L2(struct pt_entry* p);

int store_logical_page (struct logical_page_t *page, struct pt_entry **page_table, int l1, int l2);
struct logical_page_t* create_logical_page(void);
struct logical_page_t* copy_logical_page (struct logical_page_t *old_page);
//################################################################
//          END REGION DATA/FN PROTOTYPES
//################################################################




/* address space struct */
struct addrspace {
#if OPT_DUMBVM
        // vaddr_t as_vbase1;
        // paddr_t as_pbase1;
        // size_t as_npages1;
        // vaddr_t as_vbase2;
        // paddr_t as_pbase2;
        // size_t as_npages2;
        // paddr_t as_stackpbase;
#else
    struct pt_entry **page_L1;
    region_list_ptr region_list;

#endif
};

/*
 * Functions in addrspace.c:
 *
 *    as_create - create a new empty address space. You need to make
 *                sure this gets called in all the right places. You
 *                may find you want to change the argument list. May
 *                return NULL on out-of-memory error.
 *
 *    as_copy   - create a new address space that is an exact copy of
 *                an old one. Probably calls as_create to get a new
 *                empty address space and fill it in, but that's up to
 *                you.
 *
 *    as_activate - make curproc's address space the one currently
 *                "seen" by the processor.
 *
 *    as_deactivate - unload curproc's address space so it isn't
 *                currently "seen" by the processor. This is used to
 *                avoid potentially "seeing" it while it's being
 *                destroyed.
 *
 *    as_destroy - dispose of an address space. You may need to change
 *                the way this works if implementing user-level threads.
 *
 *    as_define_region - set up a region of memory within the address
 *                space.
 *
 *    as_prepare_load - this is called before actually loading from an
 *                executable into the address space.
 *
 *    as_complete_load - this is called when loading from an executable
 *                is complete.
 *
 *    as_define_stack - set up the stack region in the address space.
 *                (Normally called *after* as_complete_load().) Hands
 *                back the initial stack pointer for the new process.
 *
 * Note that when using dumbvm, addrspace.c is not used and these
 * functions are found in dumbvm.c.
 */

struct addrspace *as_create(void);
int               as_copy(struct addrspace *src, struct addrspace **ret);
void              as_activate(void);
void              as_deactivate(void);
void              as_destroy(struct addrspace *);

int               as_define_region(struct addrspace *as,
                                   vaddr_t vaddr, size_t sz,
                                   int readable,
                                   int writeable,
                                   int executable);
int               as_prepare_load(struct addrspace *as);
int               as_complete_load(struct addrspace *as);
int               as_define_stack(struct addrspace *as, vaddr_t *initstackptr);


/*
 * Functions in loadelf.c
 *    load_elf - load an ELF user program executable into the current
 *               address space. Returns the entry point (initial PC)
 *               in the space pointed to by ENTRYPOINT.
 */

int load_elf(struct vnode *v, vaddr_t *entrypoint);


#endif /* _ADDRSPACE_H_ */
