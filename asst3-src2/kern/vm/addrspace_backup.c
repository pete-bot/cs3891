/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <proc.h>

//#define DEBUG_KPRINT_FT

static struct spinlock address_lock = SPINLOCK_INITIALIZER;
struct spinlock table_lock;

static struct pt_entry* copy_L1(struct pt_entry* old );
void clean_L2(struct pt_entry* p);
void init_page_L2(struct logical_page_t **page_L2);
void init_page_L1(struct pt_entry **page_L1);
static void destroy_L1(struct pt_entry* entry);


//********************************************
//				PAGE TABLE/ AS FUNCTIONS
//********************************************

// create an address space
struct addrspace *as_create(void)
{
	struct addrspace *as;
	
	// allocate space for the addrspace struct
	as = (struct addrspace*) kmalloc(sizeof(struct addrspace));
	if (as == NULL) {
		return NULL;
	}
	
	// allocate region for the page table L1 level
	as->page_L1 = (struct pt_entry**)kmalloc(sizeof(struct pt_entry*)*(NUM_PAGES));
	
	// clean up after a failed allocation
	if(as->page_L1 == NULL){
		kfree(as);
		return NULL;
	}

	// init L1 page, create region list
	init_page_L1(as->page_L1);
	as->region_list = create_region_list();
	
	// clean up after a failed allocation
	if(as->region_list == NULL) {
		kfree(as->page_L1);
		kfree(as);
		return NULL;
	}

	return as;
}

// copy an adressspace into a new adress space
int as_copy(struct addrspace *old, struct addrspace **ret)
{
	
	// check if the 'old' addresspace is NULL, if so, return	
	if(old == NULL) {
		return ENOMEM;
	} 

	// create a new as
	struct addrspace *newas;
	newas = as_create();
	
	if (newas==NULL) {
		return ENOMEM;
	}


	spinlock_acquire(&table_lock);

	// walk old as, copy across contents, 
	// creating new objects if needed (page etc) 
	for(int i = 0; i < NUM_PAGES; ++i){
		if( (old->page_L1[i]) != NULL){
			newas->page_L1[i] = copy_L1(old->page_L1[i]);  
			//KASSERT(newas->page_L1[i] != NULL);
			// clean up after ourselves - destroy newas 
			if( (newas->page_L1[i]) == NULL) {
				as_destroy(newas);
				spinlock_release(&table_lock);
				return ENOMEM;	
			}
		}
	}
	spinlock_release(&table_lock);

	int res = copy_region_list(old->region_list, newas->region_list);
	if(res != 0) {
		as_destroy(newas);
		return res;
	}

	*ret = newas;
	return 0;
}

// destroy an address space
void as_destroy(struct addrspace *as)
{
	
	if(as == NULL) {
		return;
	}
	spinlock_acquire(&table_lock);

	//destory level 1 page table
	//walk as page table, destroy pt_entry objects
	for(int i = 0; i < NUM_PAGES; ++i) {
		if(as->page_L1[i] != NULL) {
			//destroy page_L1[i] pt_entry object
			destroy_L1(as->page_L1[i]);
		}

	}

	
	// destroy region list
	destroy_region_list(as->region_list);
	kfree(as->region_list);
	
	kfree(as->page_L1);
	// destory address space
	kfree(as);

	spinlock_release(&table_lock);

}

void as_activate(void)
{
	
	int i, spl;
	struct addrspace *as;

	// get the 'current' address space
	as = proc_getas();
	if (as == NULL) {
		return;
	}

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}

	splx(spl);
}

void as_deactivate(void)
{
	// do nothing
}


// define regions for each address space.
int as_define_region(struct addrspace *as, vaddr_t vaddr, size_t memsize,
		 int readable, int writeable, int executable)
{

	// define our region/locations based on the amount of memory
	// that our device has and the size of each page frame
	memsize += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;
	memsize = (memsize + PAGE_SIZE -1)&PAGE_FRAME;
	size_t npages = memsize/PAGE_SIZE;

	spinlock_acquire(&address_lock);

	//check that there are no overlaps
	if(base_region_exists(vaddr,as->region_list)) {
		spinlock_release(&address_lock);
		return EFAULT;
	}
	
	// set permissions for the new region in adress_space  
	int permissions = 0;

	// set permissions value correctly
	if(readable){SET_READABLE(permissions);}
	if(writeable){SET_WRITEABLE(permissions);}
	if(executable){SET_EXECUTABLE(permissions);}
	
	// insert regions into our regions list (see insert regions function)
	region_ptr new_region = insert_region(as->region_list, 
			vaddr, memsize, permissions,npages);
	if(new_region == NULL) {
		spinlock_release(&address_lock);
		return EFAULT;
	}

	spinlock_release(&address_lock);
	return 0;
}

// prepare load sets regions to be writeable, readable and executable
int as_prepare_load(struct addrspace *as)
{
	spinlock_acquire(&address_lock);	

	// for each region, back up current permissions
	// update new permissions
	region_ptr curr = as->region_list->first;
	while(curr != NULL) {
		curr->prev_permissions = curr->permissions;
		curr->permissions = (REGION_WRITEABLE | REGION_READABLE | REGION_EXECUTABLE);
		curr = curr->next;
	}
	spinlock_release(&address_lock);
	return 0;
}

// set permissions back to their previous values (pre-as_prepare_load)
int as_complete_load(struct addrspace *as)
{
	spinlock_acquire(&address_lock);

	region_ptr curr = as->region_list->first;
	while(curr != NULL) {
		curr->permissions = curr->prev_permissions;
		curr->prev_permissions = 0;
		curr = curr->next;
	}
	spinlock_release(&address_lock);

	return 0;
}

// define the stack pointer for the current addressspace. 
int as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
	as->region_list->stack = create_node(USERSTACK-(DEFAULT_STACK*PAGE_SIZE), DEFAULT_STACK*PAGE_SIZE, 
		 	(REGION_EXECUTABLE | REGION_WRITEABLE | REGION_READABLE),DEFAULT_STACK);

	if(as->region_list->stack == NULL) return EFAULT;

	/* Initial user-level stack pointer */
	*stackptr = USERSTACK;

	return 0;
}



//################################################################
//              REGION DATA/FN
//################################################################

// copy a region list from one region list to another 
int copy_region_list(region_list_ptr old, region_list_ptr new_list )
{
	region_ptr new_region;
	region_ptr curr = old->first;
	while(curr != NULL) {

		// insert a new region, using the 'old' region data
		new_region = insert_region(new_list, curr->base, curr->size,curr->permissions,curr->num_pages);
		if(new_region == NULL) {
			return ENOMEM;
		}
		curr = curr->next;
	}

	if(old->stack != NULL){
		new_list->stack = create_node(old->stack->base, old->stack->size, 
				old->stack->permissions, old->stack->num_pages);
	}

	if(old->heap != NULL){
		new_list->heap = create_node(old->heap->base, old->heap->size, 
				old->heap->permissions, old->heap->num_pages);
	}

   new_list->heap_start = old->heap_start;
   new_list->heap_end = old->heap_end;

	return 0;
}

// iterate through the region list and free all nodes
void destroy_region_list(region_list_ptr l)
{
	struct region_t *curr = l->first;
	struct region_t *prev;
	
	while(curr != NULL){
		prev = curr;
		curr = curr->next;
		kfree(prev);
	}


	// NEED TO FREE HEAP/STACK
	kfree(l->heap);
	kfree(l->stack);

	l->first = NULL;
	l->last = NULL;
	l->num_regions = 0;	
	l->heap = NULL;
	l->stack = NULL;
}


// create a simple region linked list header 
region_list_ptr create_region_list(void)
{
	region_list_ptr new_region_list = (region_list_ptr)kmalloc(sizeof(struct region_list_t));
	if(new_region_list == NULL) return NULL;

	new_region_list-> num_regions = 0;
	new_region_list-> first = NULL;
	new_region_list-> last = NULL;

	// set our special regions
	new_region_list->heap = NULL;
	new_region_list->stack = NULL;
   
   new_region_list->heap_start = 0;
   new_region_list->heap_end = 0;

	return new_region_list;

}

// helper function to create new nodes for region list
region_ptr create_node(vaddr_t base_in, size_t size_in, int permissions_in, size_t n_pages)
{
    region_ptr new_region = kmalloc(sizeof(struct region_t));
    
    if(new_region == NULL){
    	return NULL;
    }

    new_region->base = base_in;
    new_region->size = size_in;
    new_region->permissions = permissions_in;
    new_region->prev_permissions = 0;
    new_region->next = NULL;
    new_region->num_pages = n_pages;

    return new_region; 
}


// insert a region into a list, return NULL on fail. 
region_ptr insert_region(region_list_ptr region_list, vaddr_t base_in, size_t size, int permissions_in, size_t num_pages)
{
    region_ptr new_region = create_node(base_in, size, permissions_in,num_pages);
    if(new_region == NULL) return NULL;
 	   
    region_ptr curr = region_list->first;


    if(region_list->first == NULL){
        region_list->first = new_region;
    } else {
    	
        while( curr->next != NULL && curr->base < base_in ){
            curr = curr->next;
        }
        region_ptr temp = curr->next;
        curr->next = new_region;
        new_region->next = temp;
    }


    if(new_region->next == NULL){
    	region_list->last = new_region;
    }

    region_list->num_regions++;

    return new_region;
}


// scan through region list and look for region with appropriate base value. 
bool base_region_exists(vaddr_t base,region_list_ptr region_list)
{	
	region_ptr curr_region = region_list->first;
	while(curr_region != NULL) {

		if(base >= curr_region->base && base <= (curr_region->base + curr_region->size)) {
			return true;
		}
		curr_region = curr_region->next;
	}

	if(region_list->stack != NULL){
		if(base >= region_list->stack->base && base 
			<= (region_list->stack->base + region_list->stack->size)) {
				return true;
		}
	}

	if(region_list->heap != NULL){
		if(base >= region_list->heap->base && base 
			<= (region_list->heap->base + region_list->heap->size)) {
				return true;
		}
	}


	return false; 	
}

// scan through region list and find specific region - return its base value
uint8_t get_region_permission(vaddr_t address, region_list_ptr region_list)
{

	region_ptr curr_region = region_list->first;


	while(curr_region != NULL) {
		if(address >= curr_region->base && address <= (curr_region->base + curr_region->size)) {
			return curr_region->permissions;
		}
		curr_region = curr_region->next;
	}

	if(region_list->stack != NULL){
		if(address >= region_list->stack->base && address 
			<= (region_list->stack->base + region_list->stack->size)) {
				return region_list->stack->permissions;
		}
	}

	if(region_list->heap != NULL){
		if(address >= region_list->heap->base && address 
			<= (region_list->heap->base + region_list->heap->size)) {
				return region_list->heap->permissions;
		}
	}

	return 0; 

}

// scan through regions and determine if vaddr is in region list 
bool is_in_region(vaddr_t address, region_list_ptr region_list) 
{

	region_ptr curr_region = region_list->first;
	while(curr_region != NULL) {
		if(address >= curr_region->base && address <= (curr_region->base + curr_region->size)) {
			return true;
		}
		curr_region = curr_region->next;
	}

	if(region_list->stack != NULL){
		if(address >= region_list->stack->base && address 
			<= (region_list->stack->base + region_list->stack->size)) {
				return true;
		}
	}

	if(region_list->heap != NULL){
		if(address >= region_list->heap->base && address 
			<= (region_list->heap->base + region_list->heap->size)) {
				return true;
		}
	}


	return false;


} 

// 'walk' the page table, determine if a page exists or not.  
bool page_walk(int *l1, int *l2, int *offset, struct pt_entry **l1_entry, struct logical_page_t **logical_page, vaddr_t vaddr) {

	struct proc *proc = curproc;

	spinlock_acquire(&table_lock);

	struct pt_entry **page_table = proc->p_addrspace->page_L1;

	int L1_ADDR = GET_L1_ADDR(vaddr);
	//KASSERT(L1_ADDR < MAX_ADDRESSABLE);
	int L2_ADDR = GET_L2_ADDR(vaddr);
	//KASSERT(L2_ADDR < MAX_ADDRESSABLE);
	int OFFSET_ADDR = GET_OFFSET_ADDR(vaddr);
	//KASSERT(OFFSET_ADDR < PAGE_SIZE);
	
	*l1 = L1_ADDR;
	*l2 = L2_ADDR;
	*offset = OFFSET_ADDR;
	
	// if L1 entry is null, return false
	if(page_table[L1_ADDR] == NULL) {
		*l1_entry = NULL;
		spinlock_release(&table_lock);
		return false;	

	} else {

		*l1_entry = page_table[L1_ADDR];
	}

	// if L2 entry is NULL, return false;
	if(page_table[L1_ADDR]->page_L2[L2_ADDR] == NULL) {
		*logical_page = NULL;
		spinlock_release(&table_lock);
		return false;
	} else {
		*logical_page = page_table[L1_ADDR]->page_L2[L2_ADDR];
	}
	spinlock_release(&table_lock);

	return true;

}


// store a page in the page table at the appropriate place
int store_logical_page (struct logical_page_t *page, struct pt_entry **page_table, int l1, int l2) {

	spinlock_acquire(&table_lock);

	if(page_table[l1] == NULL) {
		page_table[l1] = kmalloc(sizeof(struct pt_entry));
		if(page_table[l1] == NULL) {
			spinlock_release(&table_lock);
			return ENOMEM;
		}
		//KASSERT(page_table[l1] != NULL);

		page_table[l1]->page_L2 = kmalloc(sizeof(struct logical_page_t*)*NUM_PAGES);
		if(page_table[l1]->page_L2 == NULL) {
			kfree(page_table[l1]);
			spinlock_release(&table_lock);
			return ENOMEM;
		}
		//KASSERT(page_table[l1]->page_L2 != NULL);
		init_page_L2(page_table[l1]->page_L2);

	}

	page_table[l1]->page_L2[l2] = page;
	//KASSERT(page_table[l1]->page_L2[l2] != NULL);
	spinlock_release(&table_lock);
	return 0;

}

// create a logical page
struct logical_page_t* create_logical_page(void) {

	vaddr_t new_va;
	struct logical_page_t *page = kmalloc(sizeof(struct logical_page_t));
	if(page == NULL) {
		return NULL;
	}

	spinlock_init(&page->page_lock);

	new_va = alloc_kpages(1);
	
	spinlock_acquire(&page->page_lock);
	page->physical_address = KVADDR_TO_PADDR(new_va);
	spinlock_release(&page->page_lock);

	return page;

}

// copy a logical page - used in as_copy
struct logical_page_t* copy_logical_page (struct logical_page_t *old_page) {

	struct logical_page_t *new_page;
	new_page = create_logical_page();
	if(new_page == NULL) {
		return NULL;
	}
	spinlock_acquire(&new_page->page_lock);
	spinlock_acquire(&old_page->page_lock);
	paddr_t new_paddr = new_page->physical_address;
	paddr_t old_paddr = old_page->physical_address;

	old_paddr &= PAGE_FRAME; //Align
	new_paddr &= PAGE_FRAME;
	memmove((void *)PADDR_TO_KVADDR(new_paddr), (const void *)PADDR_TO_KVADDR(old_paddr), PAGE_SIZE);

	spinlock_release(&old_page->page_lock);
	spinlock_release(&new_page->page_lock);

	return new_page;

}

//################################################################
//              END REGION DATA/FN PROTOTYPES
//################################################################


//*******************************************
//				HELPER FUNCTIONS  
//********************************************


static struct pt_entry* copy_L1(struct pt_entry* old )
{
	// create new L2 array 
	if(old == NULL) return NULL;

	// create a new page table entry 
	struct pt_entry *new_pt_entry = kmalloc(sizeof(struct pt_entry));	
	if(new_pt_entry == NULL) return NULL;

	// allocate a new L2 for our page table;
	new_pt_entry->page_L2 = kmalloc(sizeof(struct logical_page_t*)*NUM_PAGES);
	if(new_pt_entry->page_L2 == NULL) {
		kfree(new_pt_entry);
		return NULL;
	}

	// init our new L2 page
	init_page_L2(new_pt_entry->page_L2);

	// go through and copy over each page in the old table, 
	// but only copy if it exists, ie!= NULL
	for(int i = 0; i < NUM_PAGES; ++i){
		if(old->page_L2[i] != NULL){
			struct logical_page_t* new_lpage = copy_logical_page(old->page_L2[i]);
			
			// failed construction - clean up
			if (new_lpage == NULL) {
				clean_L2(new_pt_entry);
				kfree(new_pt_entry);
				return NULL;
			}
			
			// lock both pages and copy over data
			spinlock_acquire(&old->page_L2[i]->page_lock);
			spinlock_acquire(&new_lpage->page_lock);
			new_pt_entry->page_L2[i] = new_lpage;
			spinlock_release(&old->page_L2[i]->page_lock);
			spinlock_release(&new_lpage->page_lock);
		}
	}
	return new_pt_entry;
}

// clean up an L2 entry on a failed allocation 
void clean_L2(struct pt_entry* p)
{
	for(int i = 0; i < NUM_PAGES; ++i){
		if(p->page_L2[i] != NULL){
			kfree(p->page_L2[i]);
		}
	}
	kfree(p->page_L2);
}

// init a page_L2 - set each pointer to NULL
void init_page_L2(struct logical_page_t **page_L2) 
{
	for(int i = 0; i < NUM_PAGES; ++i) {
		page_L2[i] = NULL;
	}
}


// init a page_L1 - set each pointer to NULL
void init_page_L1(struct pt_entry **page_L1) 
{
	for(int i = 0; i < NUM_PAGES; ++i) {
		page_L1[i] = NULL;
	}
}


// destroy an L1
static void destroy_L1(struct pt_entry* entry) {
	// loop through entry page_L2 array and destroy logical page objects
	for(int i = 0; i < NUM_PAGES; ++i) {
		if(entry->page_L2[i] != NULL) {
			// lock pages and then destroy them.
			spinlock_acquire(&(entry->page_L2[i]->page_lock));
			
			
			free_kpages(PADDR_TO_KVADDR(entry->page_L2[i]->physical_address));
			

			spinlock_release(&(entry->page_L2[i]->page_lock));
			kfree(entry->page_L2[i]);
		}
	}
	// free level 2 page table
	kfree(entry->page_L2);
}
