#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <synch.h>
#include <mips/tlb.h>

//#define DEBUG_KPRINT_FT


/* Place your frametable data-structures here 
 * You probably also want to write a frametable initialisation
 * function and call it from vm_bootstrap
 */

// OUR VARIABLES 
static paddr_t begin_useable_mem = 0;
static paddr_t end_useable_mem = 0;

struct frametable_entry {
	bool free;
	bool fixed;
};

static struct frametable_entry * frame_table = 0;  
static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;
struct spinlock frame_lock = SPINLOCK_INITIALIZER;

/* Note that this function returns a VIRTUAL address, not a physical 
 * address
 * WARNING: this function gets called very early, before
 * vm_bootstrap().  You may wish to modify main.c to call your
 * frame table initialisation function, or check to see if the
 * frame table has been initialised and call ram_stealmem() otherwise.
 */
vaddr_t alloc_kpages(unsigned int npages)
{
  	// if the number pages is not 1, return NULL
	if(npages > 1) return 0;

	paddr_t addr;

	// first allocation - call bump allocator 
	if(frame_table == 0){
	
		spinlock_acquire(&stealmem_lock);
		addr = ram_stealmem(npages);
		spinlock_release(&stealmem_lock);

		if(addr == 0) return 0;
		return PADDR_TO_KVADDR(addr);
	}
	
	spinlock_acquire(&frame_lock);

	// we will scan through our page table to identify the first empty/available slot
	uint32_t curr = 0;
	bool found = false;
	for( curr = begin_useable_mem; curr <= end_useable_mem; ++curr){
		if(frame_table[curr].free && !frame_table[curr].fixed){
			frame_table[curr].free = false;
			frame_table[curr].fixed = false;
			found = true;
			break;	
		}
	} 

	#ifdef DEBUG_KPRINT_FT
  		kprintf("Allocating page: %d\n", curr);
  	#endif

	spinlock_release(&frame_lock);
	
	if(found == false) {
		return 0;
	}

	return PADDR_TO_KVADDR(curr*PAGE_SIZE);
}


void free_kpages(vaddr_t addr)
{
	spinlock_acquire(&frame_lock);
	uint32_t index = KVADDR_TO_PADDR(addr)/PAGE_SIZE;
	if(index < begin_useable_mem || index > end_useable_mem || frame_table[index].fixed || frame_table[index].free){
		spinlock_release(&frame_lock);
		return;
	} 
	
	
	#ifdef DEBUG_KPRINT_FT
  		kprintf("Freeing page: %d\n", index);
	#endif
	frame_table[index].free = true;
	memset((void *)PADDR_TO_KVADDR(index*PAGE_SIZE), 0, PAGE_SIZE);

	spinlock_release(&frame_lock);
	
}

void init_frametable(void) {

	
	// init other data structures
	spinlock_acquire(&stealmem_lock);

	paddr_t top_of_ram = ram_getsize();

	paddr_t location = top_of_ram - ( (top_of_ram/PAGE_SIZE) * sizeof(struct frametable_entry));
	frame_table = (struct frametable_entry *) PADDR_TO_KVADDR(location);
	
	int frametable_curr;
	
	//  allocate space for the meta page table data
	for(frametable_curr = location/PAGE_SIZE; 
			(unsigned int)frametable_curr < top_of_ram/PAGE_SIZE; 
				frametable_curr++) {
		frame_table[frametable_curr].free = false;
		frame_table[frametable_curr].fixed = true;
	}
		
	paddr_t first_available = ram_getfirstfree();

	// allocate/init space for OS161 base data
	for(frametable_curr = 0; 
		(unsigned int)frametable_curr <= first_available/PAGE_SIZE; 
			frametable_curr++) {
		
		frame_table[frametable_curr].free = false;
		frame_table[frametable_curr].fixed = true;    
	}

	// set our begin/end mem values
	begin_useable_mem = (first_available/PAGE_SIZE)+1;
	end_useable_mem = (location/PAGE_SIZE)-1;

	// set remaining memory
	for(frametable_curr = (first_available/PAGE_SIZE)+1; 
		(unsigned int)frametable_curr < location/PAGE_SIZE; 
			frametable_curr++) {
		
		frame_table[frametable_curr].free = true;
		frame_table[frametable_curr].fixed = false;    
	}

	spinlock_release(&stealmem_lock);
	
}
