#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <machine/tlb.h>
#include <current.h>
#include <proc.h>
#include <spl.h>

/* Place your page table functions here */
static struct spinlock vm_lock;
struct spinlock frame_lock;
struct spinlock table_lock = SPINLOCK_INITIALIZER;

void vm_bootstrap(void)
{
    /* Initialise VM sub-system.  You probably want to initialise your 
       frame table here as well.
    */
    //Initialise frame table
    init_frametable(); 
    spinlock_init(&vm_lock);

}

static void tlb_address_insert(paddr_t paddr, vaddr_t vaddr, uint8_t permission) {

	uint32_t hi, lo;
	spinlock_acquire(&frame_lock);
	int probe = tlb_probe(vaddr & PAGE_FRAME,0);
	/*
	if(probe < 0) {

		for(int i = 0; i < NUM_TLB; i++) {
			tlb_read(&hi, &lo, i);
			if(lo & TLBLO_VALID) {
				continue;
			}
			probe = i;
			break;
		}

	}
	*/
	hi = vaddr & TLBHI_VPAGE;
	lo = (paddr & TLBLO_PPAGE) | TLBLO_VALID;
	if(GET_WRITEABLE(permission)) {
		lo |= TLBLO_DIRTY;
	}

	if(probe < 0) {
		tlb_random(hi,lo);
	} else {
		tlb_write(hi,lo, probe);
	}

	spinlock_release(&frame_lock);

}




void lock_logical_page(struct logical_page_t *page) {
	spinlock_acquire(&page->page_lock);
}

void unlock_logical_page(struct logical_page_t *page) {
	spinlock_release(&page->page_lock);
}

struct logical_page_t* create_logical_page(void) {

	paddr_t pa;
	vaddr_t va;
	struct logical_page_t *page = kmalloc(sizeof(struct logical_page_t));
	if(page == NULL) {
		return NULL;
	}

	spinlock_init(&page->page_lock);

	va = alloc_kpages(1);
	pa = KVADDR_TO_PADDR(va);

	lock_logical_page(page);
	page->physical_address = pa;
	//page->virtual_address = va;
	unlock_logical_page(page);

	return page;

}

struct logical_page_t* copy_logical_page (struct logical_page_t *old_page) {

	struct logical_page_t *new_page;
	new_page = create_logical_page();
	if(new_page == NULL) {
		return NULL;
	}
	lock_logical_page(new_page);
	lock_logical_page(old_page);
	paddr_t new_paddr = new_page->physical_address;
	paddr_t old_paddr = old_page->physical_address;

	old_paddr &= PAGE_FRAME;

	//kprintf("Old paddr: %d\n",old_page->physical_address);
	//kprintf("Old vaddr: %d\n",old_page->virtual_address);
	//kprintf("New paddr: %d\n",new_page->physical_address);
	//kprintf("New vaddr: %d\n",new_page->virtual_address);

	memmove((void *)PADDR_TO_KVADDR(new_paddr), (const void *)PADDR_TO_KVADDR(old_paddr), PAGE_SIZE);

	unlock_logical_page(old_page);
	unlock_logical_page(new_page);

	return new_page;

}




int
vm_fault(int faulttype, vaddr_t faultaddress)
{

    struct proc *proc = curproc;
    uint8_t region_permission = get_region_permission(faultaddress, proc->p_addrspace->region_list); 
    
    struct logical_page_t *logical_page = NULL;

    if(faultaddress > MIPS_KSEG0) {
		return EFAULT;
	}

    if(!is_in_region(faultaddress, proc->p_addrspace->region_list)) {
		return EFAULT;
	}
    int l1, l2, offset; //The indices in the page table corresponding the virtual address
    struct pt_entry *l1_entry = NULL;
    bool found = page_walk(&l1,&l2, &offset, &l1_entry, &logical_page, faultaddress);

    if(!found) {
    	logical_page = create_logical_page();
    	lock_logical_page(logical_page);
    	logical_page->virtual_address = faultaddress;
    	unlock_logical_page(logical_page);
    	int res = store_logical_page (logical_page,  proc->p_addrspace->page_L1, l1, l2);
    	if(res != 0) {
    		return res;
    	}

    }
    lock_logical_page(logical_page);
	paddr_t paddr = logical_page->physical_address & PAGE_FRAME;
	unlock_logical_page(logical_page);
    if(faulttype == VM_FAULT_READ || faulttype == VM_FAULT_WRITE) {
    	tlb_address_insert(paddr, faultaddress,region_permission);
    } else {
    	if(!GET_WRITEABLE(region_permission)) {
    		return EFAULT;
		}
    	tlb_address_insert(paddr, faultaddress,region_permission);
    }

    return 0;




    /*
	// VM_FAULT_READONLY:
	// If write occured to read only entry, check that the region is infact read only.
	// If read-only, trigger error. If is in fact writeable, reinsert into tlb with
	// dirty bit set
    if(faulttype == VM_FAULT_READONLY) {


        if(!GET_WRITEABLE(region_permission)) {
            return EFAULT;
        } 

        int tlb_index = tlb_probe(faultaddress, 0);
        if(tlb_index == -1) { //Defense
            return EFAULT;
        }

        spinlock_acquire(&frame_lock);
        uint32_t high = 0;
        uint32_t low = 0;
        tlb_read(&high,&low,tlb_index);
        tlb_write(high, low|TLBLO_DIRTY,tlb_index);
        spinlock_release(&frame_lock);

        return 0;
    }

    //Sanity check - Make sure the virtual address falls in the KUSEG region
    if(faultaddress > MIPS_KSEG0) {
        return EFAULT;
    }

    int l1, l2, offset; //The indices in the page table corresponding the virtual address 
    struct pt_entry *l1_entry = NULL;
    struct logical_page_t *logical_page = NULL;
    bool found = page_walk(&l1,&l2, &offset, &l1_entry, &logical_page, faultaddress);

    //Page entry was found in the page table. Set the tlb entry with permission
    if(found) {
        paddr_t p_address = logical_page->physical_address;

        spinlock_acquire(&frame_lock);
        int probe = tlb_probe(faultaddress & TLBHI_VPAGE,0);
        uint32_t hi, lo;
        hi = faultaddress & TLBHI_VPAGE;
        lo = (p_address & TLBLO_PPAGE);
        lo |= TLBLO_VALID;
        if(GET_WRITEABLE(region_permission)) {
            lo |= TLBLO_DIRTY;
        }

        if(probe < 0) {
            tlb_random(hi,lo);
        } else {
            tlb_write(hi,lo, probe);
        }
        spinlock_release(&frame_lock);
        return 0;

    } 

    if(!is_in_region(faultaddress, proc->p_addrspace->region_list)) {
        return EFAULT;
    }

    struct pt_entry **page_table = proc->p_addrspace->page_L1;
    if(l1_entry == NULL) {
        
        page_table[l1] = kmalloc(sizeof(struct pt_entry));
        if(page_table[l1] == NULL) {
            return ENOMEM;
        }
        KASSERT(page_table[l1] != NULL);

        page_table[l1]->page_L2 = kmalloc(sizeof(struct logical_page_t*)*NUM_PAGES);
        if(page_table[l1]->page_L2 == NULL) {
            kfree(page_table[l1]);
            return ENOMEM;
        }

        KASSERT(page_table[l1]->page_L2 != NULL);
    }

    if(logical_page == NULL) {

        page_table[l1]->page_L2[l2] = kmalloc(sizeof(struct logical_page_t));
        if(page_table[l1]->page_L2[l2] == NULL) {
            return ENOMEM;
        }

        KASSERT(page_table[l1]->page_L2[l2] != NULL);

        vaddr_t virt_new_page = alloc_kpages(1);
        paddr_t physical_new_page = KVADDR_TO_PADDR(virt_new_page);
        page_table[l1]->page_L2[l2]->physical_address = physical_new_page;
        spinlock_init(&(page_table[l1]->page_L2[l2]->page_lock));



    }

    spinlock_acquire(&frame_lock);
    paddr_t p_address = page_table[l1]->page_L2[l2]->physical_address;
    int probe = tlb_probe(faultaddress & TLBHI_VPAGE,0);
    uint32_t hi, lo;
    hi = faultaddress & TLBHI_VPAGE;
    lo = (p_address & TLBLO_PPAGE);
    lo |= TLBLO_VALID;
    if(GET_WRITEABLE(region_permission)) {
        lo |= TLBLO_DIRTY;
    }

    if(probe < 0) {
        tlb_random(hi,lo);
    } else {
        tlb_write(hi,lo, probe);
    }
    spinlock_release(&frame_lock);
    return 0;
    */
}

/*
 *
 * SMP-specific functions.  Unused in our configuration.
 */

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("vm tried to do tlb shootdown?!\n");
}

