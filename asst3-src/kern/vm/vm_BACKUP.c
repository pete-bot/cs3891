#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <machine/tlb.h>
#include <current.h>
#include <proc.h>
#include <spl.h>

/* Place your page table functions here */
static struct spinlock vm_lock;
struct spinlock table_lock;


void vm_bootstrap(void)
{
    /* Initialise VM sub-system.  You probably want to initialise your 
       frame table here as well.
    */

    //Initialise frame table
    init_frametable(); 
    spinlock_init(&vm_lock);

}

void insert_into_tlb(paddr_t insert_paddr, vaddr_t insert_vaddr, uint8_t permission) {

    uint32_t hi;
    uint32_t lo;

    spinlock_acquire(&frame_lock);
    int probe = tlb_probe(insert_vaddr & PAGE_FRAME, 0);

    hi = insert_vaddr & TLBHI_VPAGE;
    lo = insert_paddr & TLBLO_PPAGE;
    if(GET_WRITEABLE(permission)) {
        lo |= TLBLO_DIRTY;
    }
    lo |= TLBLO_VALID;

    if(probe < 0) {
        tlb_random(hi,lo);
    } else {
        tlb_write(hi,lo, probe);
    }

    spinlock_release(&frame_lock);

}

int
vm_fault(int faulttype, vaddr_t faultaddress)
{
    

    struct proc *proc = curproc;
    uint8_t region_permission = get_region_permission(faultaddress, proc->p_addrspace->region_list); 
    
    struct logical_page_t *logical_page = NULL;

    //Sanity check - Make sure the virtual address falls in the KUSEG region
    if(faultaddress > MIPS_KSEG0) {   
        return EFAULT;
    }

    if(!is_in_region(faultaddress, proc->p_addrspace->region_list)) {
        return EFAULT;
    }



    int l1, l2, offset; //The indices in the page table corresponding the virtual address 
    struct pt_entry *l1_entry = NULL;
    bool found = page_walk(&l1,&l2, &offset, &l1_entry, &logical_page, faultaddress);

    if(!found) {
        logical_page = create_logical_page();
        spinlock_acquire(&logical_page->page_lock);
        logical_page->virt_address = faultaddress;
        spinlock_release(&logical_page->page_lock);
        int res = store_logical_page(logical_page, proc->p_addrspace->page_L1, l1 , l2);    
        if(res != 0) {
            return res;
        }
    }

    spinlock_acquire(&logical_page->page_lock);
    paddr_t paddr_tlb = logical_page->phys_address & PAGE_FRAME;
    spinlock_release(&logical_page->page_lock);

    if(faulttype == VM_FAULT_READONLY) {
        if(!GET_WRITEABLE(region_permission)) {
            return EFAULT;
        } 
        insert_into_tlb(paddr_tlb, faultaddress, region_permission);
    } else {
        insert_into_tlb(paddr_tlb, faultaddress, region_permission);
    }

    return 0;

} 

    /*
    // VM_FAULT_READONLY:
    // If write occured to read only entry, check that the region is infact read only.
    // If read-only, trigger error. If is in fact writeable, reinsert into tlb with
    // dirty bit set
    if(faulttype == VM_FAULT_READONLY) {


        if(!GET_WRITEABLE(region_permission)) {
            //spinlock_release(&vm_lock);
            return EFAULT;
        } 

        int tlb_index = tlb_probe(faultaddress, 0);
        if(tlb_index == -1) { //Defense
            //spinlock_release(&vm_lock);
            return EFAULT;
        }

        spinlock_acquire(&vm_lock);
        uint32_t high = 0;
        uint32_t low = 0;
        tlb_read(&high,&low,tlb_index);
        tlb_write(high, low|TLBLO_DIRTY,tlb_index);
        spinlock_release(&vm_lock);

        return 0;
    }

    //Page entry was found in the page table. Set the tlb entry with permission

    struct pt_entry **page_table = proc->p_addrspace->page_L1;
    if(l1_entry == NULL) {
        
        page_table[l1] = kmalloc(sizeof(struct pt_entry));
        if(page_table[l1] == NULL) {
            
            //spinlock_release(&vm_lock);
            return ENOMEM;
        }
        KASSERT(page_table[l1] != NULL);

        page_table[l1]->page_L2 = kmalloc(sizeof(struct logical_page_t*)*NUM_PAGES);
        if(page_table[l1]->page_L2 == NULL) {
            kfree(page_table[l1]);
            
            //spinlock_release(&vm_lock);
            return ENOMEM;
        }

        KASSERT(page_table[l1]->page_L2 != NULL);
    }

    if(logical_page == NULL) {

        page_table[l1]->page_L2[l2] = kmalloc(sizeof(struct logical_page_t));
        if(page_table[l1]->page_L2[l2] == NULL) {
            
            //spinlock_release(&vm_lock);
            return ENOMEM;
        }

        KASSERT(page_table[l1]->page_L2[l2] != NULL);

        vaddr_t virt_new_page = alloc_kpages(1);
        paddr_t physical_new_page = KVADDR_TO_PADDR(virt_new_page);
        page_table[l1]->page_L2[l2]->physical_address = physical_new_page;

    }

    paddr_t p_address = page_table[l1]->page_L2[l2]->physical_address;

    spinlock_acquire(&vm_lock);
    int probe = tlb_probe(faultaddress & TLBHI_VPAGE,0);
    uint32_t hi, lo;
    hi = faultaddress & TLBHI_VPAGE;
    lo = (p_address & TLBLO_PPAGE);
    lo |= TLBLO_VALID;
    if(GET_WRITEABLE(region_permission)) {
        lo |= TLBLO_DIRTY;
    }

    if(probe < 0) {
        tlb_random(hi,lo);
    } else {
        tlb_write(hi,lo, probe);
    }

    spinlock_release(&vm_lock);
    return 0;
}
*/

/*
 *
 * SMP-specific functions.  Unused in our configuration.
 */

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("vm tried to do tlb shootdown?!\n");
}

