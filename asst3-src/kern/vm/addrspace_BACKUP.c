/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <proc.h>

//#define DEBUG_KPRINT_FT
static void init_page_L2(struct logical_page_t **page_L2);
static void init_page_L1(struct pt_entry **page_L1);

static struct spinlock address_lock = SPINLOCK_INITIALIZER;

/*
 * Note! If OPT_DUMBVM is set, as is the case until you start the VM
 * assignment, this file is not compiled or linked or in any way
 * used. The cheesy hack versions in dumbvm.c are used instead.
 *
 * UNSW: If you use ASST3 config as required, then this file forms
 * part of the VM subsystem.
 *
 */


// TODO - edit appropriatly to incorporate these extra fns
//*******************************************
//				HELPER FUNCTIONS  
//********************************************


static struct pt_entry* copy_L1(struct pt_entry* old )
{
	// create new L2 array 
	if(old == NULL) return NULL;

	struct pt_entry *new_pt_entry = kmalloc(sizeof(struct pt_entry));	
	if(new_pt_entry == NULL) return NULL;

	// tentative
	new_pt_entry->page_L2 = kmalloc(sizeof(struct logical_page_t*)*NUM_PAGES);
	if(new_pt_entry->page_L2 == NULL) {
		// Need to clean up new_pt_entry
		return NULL;
	}		
	init_page_L2(new_pt_entry->page_L2);

	for(int i = 0; i < NUM_PAGES; ++i){
		
		if(old->page_L2[i] != NULL){

			struct logical_page_t* new_lpage = copy_logical_page(old->page_L2[i]);
			if(new_lpage == NULL) return NULL;
			spinlock_acquire(&old->page_L2[i]->page_lock);
			spinlock_acquire(&new_lpage->page_lock);

			vaddr_t new_virtual_address = old->page_L2[i]->virt_address;
			new_lpage->virt_address = new_virtual_address;
			new_pt_entry->page_L2[i] = new_lpage;

			spinlock_release(&old->page_L2[i]->page_lock);
			spinlock_release(&new_lpage->page_lock);				

			/*
			struct logical_page_t* new_lpage =  kmalloc(sizeof(struct logical_page_t));
			if (new_lpage == NULL) return NULL;

			paddr_t new_physical_address =  old->page_L2[i]->physical_address;
			new_lpage->physical_address = new_physical_address;
			new_pt_entry->page_L2[i] = new_lpage;
			*/
		}
	}
	return new_pt_entry;
}

static void init_page_L2(struct logical_page_t **page_L2) {
	for(int i = 0; i < NUM_PAGES; ++i) {
		page_L2[i] = NULL;
	}
}


static void init_page_L1(struct pt_entry **page_L1) {
	for(int i = 0; i < NUM_PAGES; ++i) {
		page_L1[i] = NULL;
	}

}


static void destroy_L1(struct pt_entry* entry) {

	//loop through entry page_L2 array and destroy logical page objects
	for(int i = 0; i < NUM_PAGES; ++i) {
		if(entry->page_L2[i] != NULL) {
			//free logical page 
			spinlock_acquire(&entry->page_L2[i]->page_lock);
			free_kpages(PADDR_TO_KVADDR(entry->page_L2[i]->phys_address));
			spinlock_release(&entry->page_L2[i]->page_lock);
			kfree(entry->page_L2[i]);
		}
	}

	//free level 2 page table
	kfree(entry->page_L2);

}



//*******************************************
//				PAGE TABLE 
//********************************************

struct addrspace *
as_create(void)
{
	struct addrspace *as;

	// allocate space for the addrspace struct
	as = (struct addrspace*) kmalloc(sizeof(struct addrspace));
	if (as == NULL) {
		return NULL;
	}

	// set the intenal varables in the addrspace struct
	// TODO - revisit in sbreak 
	as->heap = NULL;
	as->stack = NULL;
	
	#ifdef DEBUG_KPRINT_FT
		kprintf("Size of L1: %d\n",sizeof(struct pt_entry)*(NUM_PAGES));
		kprintf("Size of L2: %d\n",sizeof(struct logical_page_t*)*(NUM_PAGES));
		kprintf("Pointer: %d\n",sizeof(struct logical_page_t*));
  	#endif

	as->page_L1 = (struct pt_entry**) kmalloc(sizeof(struct pt_entry*)*(NUM_PAGES));
	if(as->page_L1 == NULL){
		//XXX Clean up required
		return NULL;
	}
	init_page_L1(as->page_L1);

	as->region_list = create_region_list();
	if(as->region_list == NULL) {
		//XXX Clean up required
		return NULL;
	}

	return as;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	// create new as - this will be returned

	struct addrspace *newas;
	newas = as_create();
	if (newas==NULL) {
		return ENOMEM;
	}

	if(old == NULL) {
		return ENOMEM;

	} 

	spinlock_acquire(&table_lock);

	// walk old as, copy across contents, 
	// creating new objects if needed (page etc) 
	for(int i = 0; i < NUM_PAGES; ++i){
		
		if( (old->page_L1[i]) != NULL){
			newas->page_L1[i] = copy_L1(old->page_L1[i]);  
			KASSERT(newas->page_L1[i] != NULL);
			if( (newas->page_L1[i]) == NULL) {
				// need to do cleanup here - Address Space XXX
				spinlock_release(&table_lock);
				return ENOMEM;	
			}
		}
	}
	spinlock_release(&table_lock);

	int res = copy_region_list(old->region_list, newas->region_list);
	if(res != 0) {
		return res;
	}

	*ret = newas;
	return 0;
}

int copy_region_list(region_list_ptr old, region_list_ptr new_list )
{

	region_ptr new_region;
	region_ptr curr = old->first;
	while(curr != NULL) {
		new_region = insert_region(new_list, curr->base, curr->size,curr->permissions,curr->num_pages);
		if(new_region == NULL) {
			return ENOMEM;
		}
		curr = curr->next;
	}

	return 0;

}

void
as_destroy(struct addrspace *as)
{
	/*
	* Clean up as needed.
	*/
	if(as == NULL) {
		return;
	}
	spinlock_acquire(&table_lock);

	//walk as page table, destroy pt_entry objects
	for(int i = 0; i < NUM_PAGES; ++i) {
		if(as->page_L1[i] != NULL) {
			//destroy page_L1[i] pt_entry object
			destroy_L1(as->page_L1[i]);
		}

	}

	//destory level 1 page table
	kfree(as->page_L1);

	//destory address space
	kfree(as);

	spinlock_release(&table_lock);

}

void
as_activate(void)
{
	

	//Patching in dumbvm.c implementation of as_activate  

	int i, spl;
	struct addrspace *as;

	as = proc_getas();
	if (as == NULL) {
		return;
	}


	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}

	splx(spl);
}

void
as_deactivate(void)
{
	/*
	 * Write this. For many designs it won't need to actually do
	 * anything. See proc.c for an explanation of why it (might)
	 * be needed.
	 */
}

/*
 * Set up a segment at virtual address VADDR of size MEMSIZE. The
 * segment in memory extends from VADDR up to (but not including)
 * VADDR+MEMSIZE.
 *
 * The READABLE, WRITEABLE, and EXECUTABLE flags are set if read,
 * write, or execute permission should be set on the segment. At the
 * moment, these are ignored. When you write the VM system, you may
 * want to implement them.
 */


// can we assume that the address space is being allocated logically?

int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t memsize,
		 int readable, int writeable, int executable)
{
	//ALIGN the region
		//Align base vaddr
		//Align size
	
	memsize += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;
	memsize = (memsize + PAGE_SIZE -1)&PAGE_FRAME;
	size_t npages = memsize/PAGE_SIZE;

	spinlock_acquire(&address_lock);

 	//check that there are no overlaps
	if(base_region_exists(vaddr,as->region_list)) {
		spinlock_release(&address_lock);
		return EFAULT;
	}

	
	// set permissions for the new region in adress_space  
	int permissions = 0;

	if(readable){SET_READABLE(permissions);}
	if(writeable){SET_WRITEABLE(permissions);}
	if(executable){SET_EXECUTABLE(permissions);}
	
	region_ptr new_region = insert_region(as->region_list, vaddr, memsize, permissions,npages);
	if(new_region == NULL) {
		spinlock_release(&address_lock);
		return EFAULT;
	}

	spinlock_release(&address_lock);
	return 0;

}

int
as_prepare_load(struct addrspace *as)
{
	
	// need to set old_perm to back up perm 
	// set new perm to writeable
	spinlock_acquire(&address_lock);

	region_ptr curr = as->region_list->first;
	while(curr != NULL) {
		curr->prev_permissions = curr->permissions;
		curr->permissions = (REGION_WRITEABLE | REGION_READABLE | REGION_EXECUTABLE);
		curr = curr->next;
	}
	spinlock_release(&address_lock);
	return 0;
}

int
as_complete_load(struct addrspace *as)
{
	spinlock_acquire(&address_lock);

	// restore perm variable to old. set old_perm to -1;
	region_ptr curr = as->region_list->first;
	while(curr != NULL) {
		curr->permissions = curr->prev_permissions;
		curr->prev_permissions = 0;
		curr = curr->next;
	}
	spinlock_release(&address_lock);

	return 0;
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
	//Create a stack region and insert into the region linked list	
	region_ptr stack_region = insert_region(as->region_list, USERSTACK-(16*PAGE_SIZE), 16*PAGE_SIZE, (REGION_EXECUTABLE | REGION_WRITEABLE | REGION_READABLE),16); 
	if(stack_region == NULL) {
		return EFAULT;
	}

	as->stack = stack_region;
	
	/* Initial user-level stack pointer */
	*stackptr = USERSTACK;

	return 0;
}



//################################################################
//              REGION DATA/FN PROTOTYPES
//################################################################




region_list_ptr create_region_list(void)
{
	region_list_ptr new_region_list = kmalloc(sizeof(struct region_list_t));
	if(new_region_list == NULL) return NULL;

	new_region_list-> num_regions = 0;
	new_region_list-> first = NULL;
	new_region_list-> last = NULL;

	return new_region_list;

}

static region_ptr create_node(vaddr_t base_in, size_t size_in, int permissions_in, size_t n_pages)
{
    region_ptr new_region = kmalloc(sizeof(struct region_t));
    
    if(new_region == NULL){
    	return NULL;
    }

    new_region->base = base_in;
    new_region->size = size_in;
    new_region->permissions = permissions_in;
    new_region->prev_permissions = 0;
    new_region->next = NULL;
    new_region->num_pages = n_pages;

    return new_region; 
}


// return region created
region_ptr insert_region(region_list_ptr region_list, vaddr_t base_in, size_t size, int permissions_in, size_t num_pages)
{

    region_ptr new_region = create_node(base_in, size, permissions_in,num_pages);
    if(new_region == NULL) return NULL;
 	   
    region_ptr curr = region_list->first;


    if(region_list->first == NULL){
        region_list->first = new_region;
    } else {
    	
        while( curr->next != NULL && curr->base < base_in ){
            curr = curr->next;
        }
        region_ptr temp = curr->next;
        curr->next = new_region;
        new_region->next = temp;
    }


    if(new_region->next == NULL){
    	region_list->last = new_region;
    }

    region_list->num_regions++;

    return new_region;
}

bool base_region_exists(vaddr_t base,region_list_ptr region_list)
{
	
	region_ptr curr_region = region_list->first;

	while(curr_region != NULL) {

		if(base >= curr_region->base && base <= (curr_region->base + curr_region->size)) {
			return true;
		}
		curr_region = curr_region->next;
	}

	return false; 	

}

uint8_t get_region_permission(vaddr_t address, region_list_ptr region_list)
{

	region_ptr curr_region = region_list->first;

	while(curr_region != NULL) {

		//kprintf("Looping~~~~\n");
		if(address >= curr_region->base && address <= (curr_region->base + curr_region->size)) {
			return curr_region->permissions;
		}
		curr_region = curr_region->next;

	}

	return 0; 

}

bool is_in_region(vaddr_t address, region_list_ptr region_list) 
{


	region_ptr curr_region = region_list->first;

	while(curr_region != NULL) {

	
		if(address >= curr_region->base && address <= (curr_region->base + curr_region->size)) {
			return true;
		}
		curr_region = curr_region->next;

	}

	return false; 

} 

bool page_walk(int *l1, int *l2, int *offset, struct pt_entry **l1_entry, struct logical_page_t **logical_page, vaddr_t vaddr) {

	struct proc *proc = curproc;

	spinlock_acquire(&table_lock);

	struct pt_entry **page_table = proc->p_addrspace->page_L1;

	int L1_ADDR = GET_L1_ADDR(vaddr);
	KASSERT(L1_ADDR < MAX_ADDRESSABLE);
	int L2_ADDR = GET_L2_ADDR(vaddr);
	KASSERT(L2_ADDR < MAX_ADDRESSABLE);
	int OFFSET_ADDR = GET_OFFSET_ADDR(vaddr);
	KASSERT(OFFSET_ADDR < PAGE_SIZE);
	

	*l1 = L1_ADDR;
	*l2 = L2_ADDR;
	*offset = OFFSET_ADDR;
	
	#ifdef DEBUG_KPRINT_FT
		kprintf("Virtual Address: %d\n", vaddr);
		kprintf("L1 ADDR: %d\n", L1_ADDR);
		kprintf("L2 ADDR: %d\n", L2_ADDR);
		kprintf("OFFSET: %d\n", OFFSET_ADDR);
  	#endif

	//XXX
	if(page_table[L1_ADDR] == NULL) {
		*l1_entry = NULL;
		spinlock_release(&table_lock);
		return false;	
	} else {
		*l1_entry = page_table[L1_ADDR];
	}

	if(page_table[L1_ADDR]->page_L2[L2_ADDR] == NULL) {
		*logical_page = NULL;
		spinlock_release(&table_lock);
		return false;
	} else {
		*logical_page = page_table[L1_ADDR]->page_L2[L2_ADDR];
	}

	spinlock_release(&table_lock);
	return true;

}

int store_logical_page(struct logical_page_t *page, struct pt_entry **page_table, int l1, int l2) {

	spinlock_acquire(&table_lock);

	if(page_table[l1] == NULL) {

		page_table[l1] = kmalloc(sizeof(struct pt_entry));
		if(page_table[l1] == NULL) {
			spinlock_release(&table_lock);
			return ENOMEM;
		}

		page_table[l1]->page_L2 = kmalloc(sizeof(struct logical_page_t*)*NUM_PAGES);
		if(page_table[l1]->page_L2) {
			kfree(page_table[l1]);
			spinlock_release(&table_lock);
			return ENOMEM;
		}
		init_page_L2(page_table[l1]->page_L2);

	}

	page_table[l1]->page_L2[l2] = page;
	KASSERT(page_table[l1]->page_L2[l2] != NULL);
	spinlock_release(&table_lock);

	return 0;

}

struct logical_page_t* create_logical_page(void) {

	
	vaddr_t frame_address;

	struct logical_page_t *new_page = kmalloc(sizeof(struct logical_page_t));
	if(new_page == NULL) {
		return NULL;
	} 

	spinlock_init(&new_page->page_lock);

	frame_address = alloc_kpages(1);
	// error 

	spinlock_acquire(&new_page->page_lock);
	new_page->phys_address = KVADDR_TO_PADDR(frame_address);
	spinlock_release(&new_page->page_lock);

	return new_page;

}

struct logical_page_t* copy_logical_page(struct logical_page_t *old_page) {
	struct logical_page_t *new_page;
	new_page = create_logical_page(); 

	if(new_page == NULL) {
		return NULL;
	}

	spinlock_acquire(&new_page->page_lock);
	spinlock_acquire(&old_page->page_lock);
	paddr_t new_paddr = new_page->phys_address;
	paddr_t old_paddr = old_page->phys_address;

	old_paddr &= PAGE_SIZE;
	memmove((void *)PADDR_TO_KVADDR(new_paddr), (const void *) PADDR_TO_KVADDR(old_paddr), PAGE_SIZE);
	
	spinlock_release(&old_page->page_lock);
	spinlock_release(&new_page->page_lock);

	return new_page;

}
//copying a logical page

//inserting a logical page into a tlb



//################################################################
//              END REGION DATA/FN PROTOTYPES
//################################################################