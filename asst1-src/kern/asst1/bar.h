
#define TRUE 1
#define FALSE 0
#define NUMBOTTLES NBOTTLES+1

//Represents a customers order
typedef struct order_s{
    struct glass *order_glass;
    struct semaphore *order_sem;
    int order_id;
} order_t;

//A buffer to store orders to be fulfilled
typedef struct buffer_s{

    order_t buffer[NCUSTOMERS];
    int head_index;
    int tail_index;
    int size;
    int customer_count; // remaining customers

} buffer_t;

//A buffer to store orders that are currently being fulfilled by bartenders
typedef struct cust_IDS_s{

    order_t buffer[NCUSTOMERS];
    int avail_elems[NCUSTOMERS];
    int head_index;
    int tail_index;
    int size;

} cust_IDS_t;
