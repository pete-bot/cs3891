#include <types.h>
#include <lib.h>
#include <synch.h>
#include <test.h>
#include <thread.h>

#include "bar_driver.h"
#include "bar.h"

// fn prototypes
void push_order_buffer(order_t new_order);
order_t pop_order_buffer(void);
void update_process_buffer_pop(order_t *v_order );

//Buffers to store orders
static buffer_t order_buffer; //Orders to be served
static cust_IDS_t process_buffer; //Order currently being fulfilled

//Mutex Locks
struct lock *buffer_lock;
struct lock *num_customer_lock; //lock for changing remaining customer count
struct cv *queue_wait;

struct semaphore *bottle_sem[NUMBOTTLES]; //Semaphore for each bottle
struct lock *bottle_check;
struct lock *bottle_change;

int bottles_in_use[NUMBOTTLES]; //State of each bottle: 0 = Free, 1 = In Use

/*
 * Push new order into the order buffer
 * Orders are stored in FIFO data structure
 */
void push_order_buffer(order_t new_order)
{
    if(order_buffer.size == NCUSTOMERS) {
        panic("bar.c: order_buffer buffer is FULL - attempted to push order onto full buffer.");
    }

    // update order buffer
    order_buffer.buffer[order_buffer.tail_index] = new_order;
    order_buffer.buffer[order_buffer.tail_index].order_id = process_buffer.avail_elems[process_buffer.head_index];    
    
    order_buffer.tail_index++;
    order_buffer.size++;

    // wrap around (if needed)
    if(order_buffer.tail_index == NCUSTOMERS){
        order_buffer.tail_index = 0;
    }

    // update process buffer
    process_buffer.head_index++;
    process_buffer.size--;
    if(process_buffer.head_index == NCUSTOMERS) {
        process_buffer.head_index = 0;
    }




}

/*
 * Takes first order from the order buffer
 */
order_t pop_order_buffer(void)
{
    if(order_buffer.size == 0) {
        panic("bar.c: order_buffer buffer is empty - attempted to pop order from empty buffer.");
    }


    order_t top_order = order_buffer.buffer[order_buffer.head_index];
    order_buffer.head_index++;
    
    if(order_buffer.head_index == NCUSTOMERS){
        order_buffer.head_index = 0;
    }

    order_buffer.size--;

    return top_order;
}

/*
 * Called after order has been fulfilled
 * Updates process buffer (cust_IDS_t), adding an order_id so that it can be reused by
 * another thread
 */
void update_process_buffer_pop(order_t *v_order )
{

    // puts order_id back on process buffer so that it can be re-used
    // by whichever customer needs to be served next
    process_buffer.avail_elems[process_buffer.tail_index] = v_order->order_id;
    process_buffer.size++;
    process_buffer.tail_index++;

    if(process_buffer.tail_index == NCUSTOMERS) {
        process_buffer.tail_index = 0;
    }
}

/*
 * **********************************************************************
 * FUNCTIONS EXECUTED BY CUSTOMER THREADS
 * **********************************************************************
 */

/*
 * order_drink()
 *
 * Takes one argument specifying the glass to be filled. The function
 * makes the glass available to staff threads and then blocks until the
 * bartender has filled the glass with the appropriate drinks.
 *
 * The glass itself contains an array of requested drinks.
 */ 
void order_drink(struct glass *glass)
{

    order_t new_order;
    new_order.order_glass = glass;
    new_order.order_sem = sem_create("new order", 0);

    //Insert into buffer
    lock_acquire(buffer_lock);

    push_order_buffer(new_order);

    cv_signal(queue_wait, buffer_lock); //Signal any bartenders waiting for a new order

    lock_release(buffer_lock);

    P(new_order.order_sem); //Customer sleeps until the order is fulfilled

    sem_destroy(new_order.order_sem);

}



/*
 * go_home()
 *
 * This function is called by customers when they go home. It could be
 * used to keep track of the number of remaining customers to allow
 * bartender threads to exit when no customers remain.
 */
void go_home(void)
{
    
    lock_acquire(num_customer_lock);
    order_buffer.customer_count--;
    lock_release(num_customer_lock);

    /*
     * Wake any bartenders that are sleep waiting for an order to be made as all customers
     * have now gone home
     */
    if(order_buffer.customer_count <= 0) {
        cv_broadcast(queue_wait,buffer_lock);
    }
        
}


/*
 * **********************************************************************
 * FUNCTIONS EXECUTED BY BARTENDER THREADS
 * **********************************************************************
 */

/*
 * take_order()
 *
 * This function waits for a new order to be submitted by customers. When
 * submitted, it records the details, and returns a pointer to something
 * representing the order.
 *
 * The return pointer type is void * to allow freedom of representation
 * of orders.
 *
 * The function can return NULL to signal the bartender thread it can now
 * exit as their are no customers nor orders left. 
 */
void * take_order(void)
{
    
    if(order_buffer.customer_count <= 0) {
        return NULL; //Go home
    }

    lock_acquire(buffer_lock);

    while(order_buffer.size == 0) {
        cv_wait(queue_wait,buffer_lock); //Wait until there is an order

        if(order_buffer.customer_count <= 0) { //Go home if there are no more customers
            cv_signal(queue_wait,buffer_lock);
            lock_release(buffer_lock);
            return NULL;
        }

    }

    order_t customer_order = pop_order_buffer();

    //Store the order in the process buffer whilst the bartender is fulfilling the order
    //Order is stored at the assigned index of customer_order.order_id
    //Storing the order in a second buffer avoids having to kmalloc() the order
    process_buffer.buffer[customer_order.order_id] = customer_order;

    lock_release(buffer_lock);
    return &process_buffer.buffer[customer_order.order_id];

}


/*
 * fill_order()
 *
 * This function takes an order generated by take_order and fills the
 * order using the mix() function to mix the drink.
 *
 * NOTE: IT NEEDS TO ENSURE THAT MIX HAS EXCLUSIVE ACCESS TO THE BOTTLES
 * IT NEEDS TO USE TO FILL THE ORDER.
 */

void fill_order(void *v)
{

    order_t *v_order = (order_t *)v;
    int i, terminate_flag, use_flag;

    lock_acquire(bottle_check);
    
    while(TRUE){
        use_flag = FALSE;
        terminate_flag = FALSE;

        for(i = 0; i < DRINK_COMPLEXITY; ++i){
            
        	// check for 0 order, ie, terminate drink order
            if(terminate_flag){
                v_order->order_glass->requested[i] = 0;
                continue;                
            }

            if(v_order->order_glass->requested[i] == 0){
                terminate_flag = TRUE;
                continue;
            }
            
            //If a requested bottle is in use
            if( bottles_in_use[v_order->order_glass->requested[i]] ){
                lock_release(bottle_check);
                P(bottle_sem[v_order->order_glass->requested[i]]); //Sleep wait until the bottle becomes free
                lock_acquire(bottle_check);
                use_flag = TRUE;
            }
        }

        //If all bottles are free
        if(!use_flag){
            break;
        }
    }
    
    //Set the state of each requested bottle to now being in use
    for(i = 0; i < DRINK_COMPLEXITY; ++i) {
        if(v_order->order_glass->requested[i] == 0) {
            continue;
        }
        bottles_in_use[v_order->order_glass->requested[i]] = 1;
    }
    lock_release(bottle_check); 
    
    mix(v_order->order_glass);

    //Set the state of each requested bottle to now being in use
    lock_acquire(bottle_check);
    for(i = 0; i < DRINK_COMPLEXITY; ++i){
        bottles_in_use[v_order->order_glass->requested[i]] = 0;
        V(bottle_sem[v_order->order_glass->requested[i]]);
    }
    lock_release(bottle_check);
}


/*
 * serve_order()
 *
 * Takes a filled order and makes it available to the waiting customer.
 */

void serve_order(void *v)
{
    (void)v;
    order_t *v_order = (order_t*)v;
    

    lock_acquire(buffer_lock);

    update_process_buffer_pop(v_order); //Make the order_id of v_order free again for new orders

    lock_release(buffer_lock);
    
    //Wake up the customer that v_order belongs to
    //The order is now ready to be consumed
    V(v_order->order_sem);


}



/*
 * **********************************************************************
 * INITIALISATION AND CLEANUP FUNCTIONS
 * **********************************************************************
 */


/*
 * bar_open()
 *
 * Perform any initialisation you need prior to opening the bar to
 * bartenders and customers
 */
void bar_open(void)
{
    // initialise and check lock/sem/cv variables
    buffer_lock = lock_create("buffer_lock");
    if( buffer_lock == NULL) { 
        panic("bar.c: buffer_lock was NULL");
    }

    num_customer_lock = lock_create("customer lock");
    if(num_customer_lock == NULL){ 
        panic("bar.c: num_customer_lock was NULL");
    }

    bottle_check = lock_create("bottle_check");
    if(bottle_check == NULL) {
        panic("bar.c bottle_Check was NULL");
    }

    bottle_change = lock_create("bottle_change");
    if(bottle_change == NULL){
        panic("bar.c bottle_Check was NULL");
    }


    queue_wait = cv_create("queue wait");
    if(queue_wait == NULL) { 
        panic("bar.c: queue_wait was NULL");
    }

    order_buffer.head_index = 0;
    order_buffer.tail_index = 0;
    order_buffer.size = 0;
    order_buffer.customer_count = NCUSTOMERS;

    // create and init drinks locks array 
    // may need unique names
    int i;
    for (i = 0; i < NUMBOTTLES; ++i){
        bottle_sem[i] = sem_create("bottle_sem_tmp", 0);
        if(bottle_sem[i] == NULL){
            panic("bar.c: bottle_sem[%d] could not allocate.", i);
        }
    }

    for(i = 0 ; i < NUMBOTTLES; i++) {
        bottles_in_use[i] = 0;
    }

    process_buffer.head_index = 0;
    process_buffer.tail_index = 0;
    process_buffer.size = 0;

    // set up the process buffer with process IDs
    for(i = 0 ; i < NCUSTOMERS; i++) {
        process_buffer.avail_elems[i] = i;
    }
}



/*
 * bar_close()
 *
 * Perform any cleanup after the bar has closed and everybody has gone home.
 */
void bar_close(void)
{

    lock_destroy(buffer_lock);
    lock_destroy(num_customer_lock);
    lock_destroy(bottle_check);
    lock_destroy(bottle_change);

    cv_destroy(queue_wait);


    int i;
    for (i = 0; i < NUMBOTTLES; ++i){
        sem_destroy(bottle_sem[i]);
    }
}
