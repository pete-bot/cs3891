/*   
 * **********************************************************************
 *
 * Define function prototypes, types, and constants needed by both the
 * driver (bar_driver.c) and the code you need to write (bar.c)
 *
 * YOU SHOULD NOT RELY ON ANY CHANGES YOU MAKE TO THIS FILE
 *
 * We will use our own version of this file for testing
 */



/*
 * Define the number of drink types available and their symbolic constants. 
 *
 */
#define BEER 1
#define VODKA 2
#define RUM 3
#define GIN 4
#define TEQUILA 5
#define BRANDY 6
#define WHISKY 7
#define BOURBON 8
#define TRIPLE_SEC 9
#define ORANGE_JUICE 10
#define NBOTTLES 10


/*
 * The maximum number of drinks that can be mixed in a single glass
 */
#define DRINK_COMPLEXITY 3

/*
 * The data type representing a glass
 */ 
struct glass {
	/* the bottles requested for this drink */
	unsigned int requested[DRINK_COMPLEXITY];

	/* the actual contents of the glass */
	unsigned int contents[DRINK_COMPLEXITY];
};


/*
 * FUNCTION PROTOTYPES FOR THE FUNCTIONS YOU MUST WRITE
 *
 * YOU CANNOT MODIFY THESE PROTOTYPES
 *  
 */

/* Customer functions */
extern void order_drink(struct glass *);
extern void go_home(void);


/* Bar staff functions */ 
extern void * take_order(void);
extern void fill_order(void *);
extern void serve_order(void *);


/* Bar opening and closing functions */
extern void bar_open(void);
extern void bar_close(void);


/*
 * Function prototype for the supplied routine that mixes the various
 * bottle contents into a glass.
 *
 * YOU MUST USE THIS FUNCTION FOR MIXING
 *
 */
extern void mix(struct glass *);


/*
 * THESE PARAMETERS CAN BE CHANGED BY US, so you should test various
 * combinations. NOTE: We will only ever set these to something
 * greater than zero.
 */ 

#define NCUSTOMERS 20 /* The number of customers drinking today */
#define NBARTENDERS 5 /* The number of bartenders working today */

