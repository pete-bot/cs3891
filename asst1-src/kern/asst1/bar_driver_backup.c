#include "opt-synchprobs.h"

#include <types.h>
#include <lib.h>
#include <synch.h>
#include <thread.h>
#include <test.h>

#include "bar_driver.h"


/*
 * DEFINE THIS MACRO TO SWITCH ON MORE PRINTING
 *
 * Note: Your solution should work whether printing is on or off
 *  
 */

/* #define PRINT_ON */

/* this semaphore is for cleaning up at the end. */
static struct semaphore *alldone;

/*
 * Data type used to track number of doses each bottle gives
 */

struct bottle {
	int doses; 
};

struct bottle bottles[NBOTTLES];



/*
 * **********************************************************************
 * CUSTOMERS
 *
 * Customers are rather simple, they arrive and give their order to a
 * bartender and wait for their drink.
 *
 * Eventually their glass arrives with the requested contents (exactly
 * as requested), they drink until the glass is empty, take a short
 * break, and do it all again until they have emptied the desired
 * number of glasses (customers passing out is not simulated).
 *
 */

static void customer(void *unusedpointer, unsigned long customernum)
{
	struct glass glass;
	int i,j;

	(void) unusedpointer; /* avoid compiler warning */


	i = 0; /* count number of interations */
	do {


#ifdef PRINT_ON
		kprintf("C %ld is ordering\n", customernum);
#endif

		/* Clear away previously-requested ingredients and select a new drink */
		for (j = 0; j < DRINK_COMPLEXITY; j++) {
			glass.requested[j] = 0;
		}

		/* I'll have a beer. */
		glass.requested[0] = BEER;

		/* order the drink, this blocks until the order is fulfilled */
		order_drink(&glass);

#ifdef PRINT_ON
		kprintf("C %ld drinking %d, %d, %d\n", customernum,
				glass.contents[0],
				glass.contents[1],
				glass.contents[2]);
#endif

		/* Drink up */
		for (j = 0; j < DRINK_COMPLEXITY; j++) {
			glass.contents[j] = 0;
		}


		/* I needed that break.... */
		thread_yield();

		i++;
	} while (i < 10); /* keep going until .... */ 

#ifdef PRINT_ON  
	kprintf("C %ld going home\n", customernum);
#else
	(void)customernum;
#endif

	/*
	 * Now we go home. 
	 */
	go_home();
	V(alldone);
}


/*
 * **********************************************************************
 * BARTENDERS
 * bartenders are only slightly more complicated than the customers.
 * They take orders, and if valid, they fill them and serve them.
 * When all the customers have left, the bartenders go home.
 *
 * An invalid order signals that the bartender should go home.
 *
 */
static void bartender(void *unusedpointer, unsigned long staff)
{

	void *o;
	int i;
	(void)unusedpointer; /* avoid compiler warning */

	i = 0; /* count orders filled for stats */
	while (1) {

#ifdef PRINT_ON
		kprintf("S %ld taking order\n", staff);
#endif

		o = take_order();

		if (o != NULL) {

#ifdef PRINT_ON
			kprintf("S %ld filling\n", staff);
#endif

			i++;
			fill_order(o);

#ifdef PRINT_ON
			kprintf("S %ld serving\n", staff);
#endif

			serve_order(o);
		}
		else {
			break;
		}

	};

	kprintf("S %ld going home after mixing %d drinks\n", staff, i);
	V(alldone);
}


/*
 * **********************************************************************
 * RUN THE BAR
 * This routine sets up the bar prior to opening and cleans up after
 * closing.
 *
 * It calls two routines (bar_open() and bar_close()) in which you can
 * insert your own initialisation code.
 *
 * It also prints some statistics at the end.
 *
 */

int runbar(int nargs, char **args)
{
	int i, result;

	(void) nargs; /* avoid compiler warnings */
	(void) args;

	/* this semaphore indicates everybody has gone home */
	alldone = sem_create("alldone", 0);
	if (alldone == NULL) {
		panic("runbar: out of memory\n");
	}

	/* initialise the bottle doses to 0 */ 
	for (i=0 ; i<NBOTTLES; i++) {
		bottles[i].doses = 0;
	}

	/***********************************************************************
	 * call your routine that initialises the rest of the bar
	 */
	bar_open();

	/* Start the bartenders */
	for (i=0; i<NBARTENDERS; i++) {
		result = thread_fork("bartender thread", NULL,
				&bartender, NULL, i);
		if (result) {
			panic("runbar: thread_fork failed: %s\n",
					strerror(result));
		}
	}

	/* Start the customers */
	for (i=0; i<NCUSTOMERS; i++) {
		result = thread_fork("customer thread", NULL,
				&customer, NULL, i);
		if (result) {
			panic("runbar: thread_fork failed: %s\n",
					strerror(result));
		}
	}

	/* Wait for everybody to finish. */
	for (i=0; i< NCUSTOMERS+NBARTENDERS; i++) {
		P(alldone);
	}

	for (i=0 ; i<NBOTTLES; i++) {
		kprintf("Bottle %d used for %d doses\n", i+1, bottles[i].doses);
	}

	/***********************************************************************
	 * Call your bar clean up routine
	 */
	bar_close();

	sem_destroy(alldone);
	kprintf("The bar is closed, bye!!!\n");
	return 0;
}



/*
 * **********************************************************************
 * MIX
 *
 * This function take a glass and an order and mixes the
 * drink as required. It does it such that the contents
 * EXACTLY matches the request.
 *
 * MIX NEEDS THE ROUTINE THAT CALLS IT TO ENSURE THAT MIX HAS EXCLUSIVE 
 * ACCESS TO THE BOTTLES IT NEEDS.
 *
 * YOU MUST USE THIS MIX FUNCTION TO FILL GLASSES
 *
 */

void mix(struct glass *c)
{
	int i;

	/*
	 * add drinks to the glass in order given and increment number of
	 * doses from particular bottle
	 */

	for (i=0; i<DRINK_COMPLEXITY; i++){
		int bottle;
		bottle = c->requested[i];
		c->contents[i] = bottle;

		if (bottle > NBOTTLES) {
			panic("Unknown bottle");
		}
		if (bottle > 0) {
			bottles[bottle-1].doses++;
		}
	}
}

