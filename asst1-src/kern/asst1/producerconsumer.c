/* This file will contain your solution. Modify it as you wish. */
#include <types.h>
#include "producerconsumer_driver.h"
#include <synch.h>
#include <thread.h>

#define BUFFER_OFFSET 1

/* This is called by a consumer to request more data. */

// function prototypes
void push_queue(struct pc_data item);
struct pc_data pop_queue(void);

//Buffer to store pc_data items producers produce to and consumers consume from
typedef struct process_buffer_s{
    int head_index;
    int tail_index;
    int size;
    struct pc_data buffer[BUFFER_SIZE];
}p_buffer_t;

// create our process buffer variables/lock variable
p_buffer_t pBuff;
struct lock *l;
struct cv *empty;
struct cv *full;

//Pushes a pc_data item onto the buffer
void push_queue(struct pc_data item)
{
    if(pBuff.size == BUFFER_SIZE) {
        panic("producerconsumer.c: pBuff buffer is FULL - attempted to push data onto full buffer.");
    }

    pBuff.buffer[pBuff.tail_index] = item;
    pBuff.tail_index++;

    //Buffer operates in a circular fashion
    if(pBuff.tail_index == BUFFER_SIZE){
        pBuff.tail_index = 0;
    }

    pBuff.size++;
}

//Pops a pc_data item from the buffer
struct pc_data pop_queue(void)
{
    if(pBuff.size == 0) {
        panic("producerconsumer.c: orders buffer is empty - attempted to pop data from empty buffer.");
    }

    struct pc_data top_data = pBuff.buffer[pBuff.head_index];
    pBuff.head_index++;
    if(pBuff.head_index == BUFFER_SIZE){
        pBuff.head_index = 0;
    }

    pBuff.size--;

    return top_data;
}


struct pc_data consumer_consume(void)
{

    struct pc_data thedata;
    
    lock_acquire(l); //Acquire lock before consuming from the buffer
    
    //Sleep wait until there is an item in the buffer
    while(pBuff.size == 0){
        cv_wait(empty, l);   
    }

    thedata = pop_queue();

    //If there is space in the buffer, wake up a producer
    if(pBuff.size < BUFFER_SIZE ){
        cv_signal(full, l);
    }

    lock_release(l);
    return thedata;

    /* OLD CODE SECTION

        thedata = pBuff.buffer[pBuff.head_index];
        pBuff.head_index++;
       

        //kprintf("consnpBuff.head: %d, pbuff.tail: %d, pbuff.size: %d\n", pBuff.head_index, pBuff.tail_index, pBuff.size );

        if( pBuff.head_index == BUFFER_SIZE ){ 
            //kprintf("cons: buffer head = BUFFER_SIZE - resetting index\n");
            pBuff.head_index = 0;
        }

        pBuff.size--;
        
        if(pBuff.size < BUFFER_SIZE ){ 
            //kprintf("cons: buffer no longer full - signal producer\n");
            cv_signal(full, l);
        }
        
        //kprintf("\n");
        lock_release(l);
    */
}

/* This is called by a producer to store data. */
void producer_produce(struct pc_data item)
{

    lock_acquire(l);

    //Sleep wait until there is room in the buffer
    while(pBuff.size == BUFFER_SIZE){ // -1){ 
        cv_wait(full, l);
    }
    push_queue(item);

    //If there is an item in the buffer, wake up a consumer thread
    if( pBuff.size > 0 ) { 
        cv_signal(empty, l); 
    }

    lock_release(l);
    

    /* OLD CODE
        if(pBuff.tail_index == BUFFER_SIZE){
            //kprintf("prod: buffer tail = BUFFER_SIZE, resetting index\n");
            pBuff.tail_index = 0;
        }

        //kprintf("pushing element to buffer\n");
        pBuff.buffer[pBuff.tail_index] = item;
        pBuff.size++;

        //kprintf("prod: pBuff.head: %d, pbuff.tail: %d, pbuff.size: %d\n", pBuff.head_index, pBuff.tail_index, pBuff.size );


        pBuff.tail_index++;    
    */
}

/* Perform any initialisation (e.g. of global data) you need here */
void producerconsumer_startup(void)
{
    empty = cv_create("empty");
    if(empty == NULL){
        panic("producerconsumer.c: empty failed to initialise.");
    }

    full = cv_create("full");
    if(full == NULL){
        panic("producerconsumer.c: full failed to initialise.");
    }

    l = lock_create("l");
    if(l == NULL){
        panic("producerconsumer.c: l failed to initialise.");
    }

    // init buffer (global)
    pBuff.head_index = 0;
    pBuff.tail_index = 0;
    pBuff.size = 0;

}

/* Perform any clean-up you need here */
void producerconsumer_shutdown(void)
{
    cv_destroy(empty);
    cv_destroy(full);
    lock_destroy(l);
}
