====================================================
    1a: math.c
====================================================

Problem: When run without modification, a counter in math.c will be accessed by multiple
threads, causing the counter to increment erratically. The critical region is in the adder() function where  the counter variable is modified. Local variables, a, b and others are not shared, because they are local variables.

Solution: A lock ('count_lock') is created. We then apply the lock 
to the critical region. This will prevent any thread from modifying the
variable while any other thread is accessing or modifying it.


====================================================
    1b: producerconsumer.c
====================================================

Problem: Part 2 of the assignment required paralellisation of the producer and consumer functions. The critical region consists of the buffer data structure that the producer will push onto, and the consumer pop off of. This data structure must also stop the producer from pushing onto a full
buffer and stop the consumer from popping off an empty buffer.

Solution: We began by creating a struct that would contain our circular
buffer the p_buffer_t (process buffer type). This contains an array of struct pc_data of size
BUFFER_SIZE as well as a head index,tail index and size values for that buffer. We also created push and pop functions to abstract the buffer.

Our producer_produce() function locks our buffer variable with a standard
lock, 'l'. We then use a conditional variable, 'full', to cv_wait() if the buffer is full. 
This will unlock the buffer and allow access
to it by consumer threads while the producer waits for space to open up.

While our buffer is full, producer will wait. Once a spot opens up,
it will push an element to the buffer. If the buffer was previously
empty, we signal on another condition variable, 'empty', to wake up any
sleeping consumer threads. We then release our lock, l.

consumer_consume() locks the buffer. If the buffer is empty, it will wait on a condition variable,
'empty'. This stops consumer_consume() from attempting to pop from
an empty buffer. If the buffer is not empty, it will pop a pc_data
element off the queue. If the buffer was previously full, it will signal the 'full' condition,
which will wake any sleeping producers so that they can start
producing again. The lock on the buffer, 'l' is released and the pc_data
element is returned.


====================================================
    1c: bar.c
====================================================

Problem: There are multiple customers (at a bar), who each produce an order(shared resource). Bartenders serve these orders, using the shared resources (bottles).

Solution:

----------------- Data Structures -----------------

bar.h constains 3 structs:
 
1. 'order_t': When customer orders a
drink they create a new order, being the order_t data structure. order_t
contains
    -A glass object ('order_glass'): describes the customers order
    -A semaphore ('order_sem'): allows the customer thread to sleep
    on their order until it is fulfilled 
    -id number ('order_id): an integer that represents an individual customer order

2. 'buffer_t': When a customer makes an order it is placed into our
buffer, from which bartenders can consume. buffer_t contains:
    -An array of orders ('buffer') 
    -Indexes for the head and tail
    positions of the buffer ('head_index' & 'tail_index') 
    -The current size of the buffer ('size') 
    -A customer count variable ('customer_count'): tracks how many customers remain.

3. 'cust_IDS_t': When a bartender takes an order from the buffer
('buffer_t'), they then place it into a 'cust_IDS_t' buffer. This allows
us to avoid any sort of dynamic memory allocation. This
data structure uses two arrays. One is a queue of id's, the other is an array of order_t objects. When we take an order and place
it into our 'cust_IDS_t' buffer we keep track of the index in the array
of which we are storing it in. We record this in the 'order_id' variable
(order_t data structure). The second array is another queue that keeps
track of available id's to which an order can be stored in the
'cust_IDS_t' buffer. Overall this allows us to avoid overwriting our
returned pointer (if we simply left it in the order buffer) and also
avoid using kmalloc(). The implementation is similar to our 'buffer_t' struct.

In our bar.c file 
1.'bottles_in_use' : An array that keeps track of the use states of all 
2. 'bottle_sem': An array of semaphores for each bottle. It allows bartenders to sleep
on unavailable bottles


----------------- functions -----------------

'order_drink()': 
-We create a new order object, update its glass member
and create new semaphore associated with the order.
-We acquire a lock on our buffer and push the new order object.  
-We then signal using a conditional variable, 'queue_wait', to wake up any bartenders
that are waiting on the buffer.  
-We release our buffer lock and call P() on the order's semaphore, allowing
the customer to sleep on the order until it's fulfilled. The bartender
will use the semaphore to wake up the customer when they have completed
the order.

'go_home()': 
-Locks the buffer and updates its customer count.  
-The lock is released and the customer count checked 
-If the customer count is 0, then all customers have gone home, so a broadcast on
our 'queue_wait' conditional variable will wake up any sleeping
bartenders, allowing them go home, since there will be no more
pushes to the buffer to wake them up.

'take_order()': 
-Firstly checks to see if the customer count is 0, and if so, returns NULL.  
-The buffer is then locked and we check to see if the
buffer is empty and sleep with a conditional variable if so. Otherwise,
inside of our loop, we check to see if the customer count is 0, if
so, we return NULL.  
-Once the buffer is not empty, we pop an order off it, store it in our 
'cust_IDS_t' buffer (also called process buffer) and release our lock, 
returning the corresponding element in our 'cust_IDS_t' buffer.

'fill_order()': 
-Locks the array of bottle semaphores.
-Iterates through all of the requested bottles in the glass. If the
bottle number is 0 (end of order), it exits. If a
bottle is in use, it will call P() on the appropriate bottle semaphore. If
all bottles are available, it will complete this loop and set the bottles
it requires to a used state. It will then release the lock.  
-We call mix() 
-We lock the array of bottle semaphores again, change the
state of the bottles we used to become free and call V() on each
bottle semaphore we used. This is to wake up any bartenders that are
waiting on the bottles.

'serve_order()': 
-Locks the 'cust_IDS_t' buffer, updates the queue of
id's to indicate that there is a new free space , releases the buffer
lock and calls V() on the order, allowing the customer to take it away.

