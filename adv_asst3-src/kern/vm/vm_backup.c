#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <machine/tlb.h>
#include <current.h>
#include <proc.h>
#include <spl.h>

/* Place your page table functions here */
static struct spinlock vm_lock;
struct spinlock frame_lock;
struct spinlock table_lock = SPINLOCK_INITIALIZER;
struct spinlock tlb_lock = SPINLOCK_INITIALIZER;

void vm_bootstrap(void)
{
    
    // initialise frame table
    init_frametable(); 
    
    // init the vm_lock spinlock 
    spinlock_init(&vm_lock);

}

void tlb_insert_address(paddr_t paddr, vaddr_t vaddr, uint8_t permission) {

	uint32_t hi, lo;
	spinlock_acquire(&tlb_lock);
	
    // probe for vaddr in tlb
    int probe = tlb_probe(vaddr & PAGE_FRAME,0);
	
	hi = vaddr & TLBHI_VPAGE;
	lo = (paddr & TLBLO_PPAGE);
	lo |= TLBLO_VALID;
	
    // set dirty bit on entrylo
    if(GET_WRITEABLE(permission)) {
		lo |= TLBLO_DIRTY;
	}

    // if probe was unsuccessful, write to random position in tlb  
	if(probe < 0) {        // probe returns negative on miss
		tlb_random(hi,lo);
	
    // else write at position returned by probe
    } else {
		tlb_write(hi,lo, probe);
	}

	spinlock_release(&tlb_lock);

}

int vm_fault(int faulttype, vaddr_t faultaddress)
{

    struct proc *proc = curproc;

    // get the current region permissions
    uint8_t region_permission = get_region_permission(faultaddress, 
            proc->p_addrspace->region_list, NOT_IGNORE_REF); 
    
    struct logical_page_t *logical_page = NULL;

    // make sure we are in the correct segment
    if(faultaddress > MIPS_KSEG0) {
		return EFAULT;
	}

    // make sure fault address is in correct region  
    if(!is_in_region(faultaddress, proc->p_addrspace->region_list)) {
		return EFAULT;
	}
    int l1, l2, offset; //The indices in the page table corresponding the virtual address
    struct pt_entry *l1_entry = NULL;
    
    // 'walk' through our page table and grab page information ( if it exists)
    bool found = page_walk(&l1,&l2, &offset, &l1_entry, &logical_page, faultaddress);

    if (!found) {

        // our walk was not successfull, so we create a new page 
    	logical_page = create_logical_page();
    	
        // update page table with newly created logical page    	
        int res = store_logical_page (logical_page,  proc->p_addrspace->page_L1, l1, l2);
    	if(res != 0) {
    		return res;
    	}

    }

    spinlock_acquire(&logical_page->page_lock);
	paddr_t paddr = logical_page->physical_address & PAGE_FRAME;
	spinlock_release(&logical_page->page_lock);
    
    // insert our newly updated address into the tlb - 
    // this will be picked up by the next tlb 'probe'/scan 
    if(faulttype == VM_FAULT_READ || faulttype == VM_FAULT_WRITE) {
    	tlb_insert_address(paddr, faultaddress,region_permission);
    } else {
        kprintf("ref_count : %d\n", get_ref_count(paddr) );
        if( get_ref_count(paddr) > 1) {
            region_permission =get_region_permission(faultaddress, 
            proc->p_addrspace->region_list, IGNORE_REF); 
            
            if(!GET_WRITEABLE(region_permission)){ return EFAULT;}

            paddr_t new_paddr = duplicate_frame(paddr);
            decrement_ref_count(paddr);
            logical_page->physical_address = new_paddr;
            paddr = new_paddr;
		} else {
             if(!GET_WRITEABLE(region_permission)){ return EFAULT;}
        }
    	tlb_insert_address(paddr, faultaddress,region_permission);
    }

    return 0;
}

/*
 * SMP-specific functions.  Unused in our configuration.
 */

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts; panic("vm tried to do tlb shootdown?!\n");
}

